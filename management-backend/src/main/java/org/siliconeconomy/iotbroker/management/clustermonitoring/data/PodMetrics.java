/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This DTO is used to transfer information about a pod's metrics.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PodMetrics {

    /**
     * The name of the pod.
     */
    private String name;
    /**
     * The label that is specific to the pod type.
     * Multiple pod instances of the same type share the same label.
     */
    private String label;
    /**
     * The actual metric values.
     */
    private PodMetricsValues values;
    /**
     * The resource limits configured for the pod.
     */
    private PodResourceLimits resourceLimits;
}
