/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring;

/**
 * Thrown to indicate that a pod with a specific name could not be found
 *
 * @author M. Grzenia
 * @version 1.0
 */
public class PodNotFoundException extends RuntimeException {

    public PodNotFoundException(String message) {
        super(message);
    }
}
