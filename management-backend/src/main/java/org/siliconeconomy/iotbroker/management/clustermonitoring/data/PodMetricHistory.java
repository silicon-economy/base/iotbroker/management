/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * This DTO is used to transfer historical information about a single metric of a pod.
 * The historical information is provided in a list where each entry is composed of a metric value and a timestamp for
 * the particular value.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class PodMetricHistory {

    /**
     * The name of the pod.
     */
    private String name;
    /**
     * A list of value and timestamp pairs for a particular pod metric.
     */
    private List<PodMetricHistoryValue> values;
}
