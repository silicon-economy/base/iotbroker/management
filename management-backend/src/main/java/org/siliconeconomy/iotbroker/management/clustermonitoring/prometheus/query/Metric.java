/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

/**
 * Defines pod metrics with their corresponding (PromQL) query strings that are used to query the
 * Prometheus HTTP API for the corresponding pod metric.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public enum Metric {

    /**
     * The receive bandwidth metric.
     */
    BANDWIDTH_RX("irate(container_network_receive_bytes_total[5m])"),
    /**
     * The transmit bandwidth metric.
     */
    BANDWIDTH_TX("irate(container_network_transmit_bytes_total[5m])"),
    /**
     * The cpu usage metric.
     */
    CPU_USAGE("pod:container_cpu_usage:sum"),
    /**
     * The memory usage metric.
     */
    MEMORY_USAGE("container_memory_working_set_bytes{container=''}");

    /**
     * The (PromQL) query string that is used to query the corresponding metric.
     */
    private String query;

    Metric(String query) {
        this.query = query;
    }

    /**
     * Returns the query string that is used to query the Prometheus HTTP API for the corresponding pod metric.
     *
     * @return The query string.
     */
    public String getQuery() {
        return query;
    }
}
