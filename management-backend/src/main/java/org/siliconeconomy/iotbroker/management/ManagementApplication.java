/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Contains the main method and thus the entry point of the java-backend application.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@SpringBootApplication
public class ManagementApplication {

    public static void main(final String[] args) {
        SpringApplication.run(ManagementApplication.class, args);
    }
}
