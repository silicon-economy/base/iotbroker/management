/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.mapper;

import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetrics;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetricsValues;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodResourceLimits;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.Metric;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.PodMetricsQuery;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.PodMetricsResult;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.ResourceLimit;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Used to map results as received by the Prometheus HTTP API to {@link PodMetrics} instances.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public class PodMetricsMapper {

    /**
     * Maps a list of {@link PodMetricsResult}s which contains entries for each and every pod metric as received by a
     * {@link PodMetricsQuery} to a single {@link PodMetrics} instance encapsulating all the metric values.
     *
     * @param podName The name of the pod the metrics belong to.
     * @param results The individual {@link PodMetricsResult}s.
     * @return The individual results mapped to a single {@link PodMetrics} instance.
     */
    public PodMetrics map(String podName, List<PodMetricsResult> results) {
        requireNonNull(podName, "podName");
        requireNonNull(results, "results");

        Map<String, List<PodMetricsResult>> metricsByName
            = results.stream().collect(Collectors.groupingBy(result -> result.getMetric().getMetricName()));

        PodMetricsValues metrics = new PodMetricsValues();
        metrics.setCpuUsage(mapCpuUsage(metricsByName));
        metrics.setMemoryUsage(mapMemoryUsage(metricsByName));
        metrics.setReceiveBandwidth(mapReceiveBandwidth(metricsByName));
        metrics.setTransmitBandwidth(mapTransmitBandwidth(metricsByName));
        metrics.setTimestamp(mapTimestamp(results));

        PodResourceLimits resourceLimits = new PodResourceLimits();
        resourceLimits.setCpu(mapCpuCoresLimit(metricsByName));
        resourceLimits.setMemory(mapMemoryLimit(metricsByName));

        PodMetrics result = new PodMetrics();
        result.setName(podName);
        result.setLabel(mapLabel(results));
        result.setValues(metrics);
        result.setResourceLimits(resourceLimits);

        return result;
    }

    private String mapLabel(List<PodMetricsResult> results) {
        return !results.isEmpty()
            ? results.get(0).getMetric().getLabelName()
            : "";
    }

    private double mapCpuUsage(Map<String, List<PodMetricsResult>> metricsByName) {
        return metricsByName.containsKey(Metric.CPU_USAGE.name())
            ? Double.parseDouble(metricsByName.get(Metric.CPU_USAGE.name()).get(0).getValue().get(1))
            : 0.0;
    }

    private long mapMemoryUsage(Map<String, List<PodMetricsResult>> metricsByName) {
        return metricsByName.containsKey(Metric.MEMORY_USAGE.name())
            ? Long.parseLong(metricsByName.get(Metric.MEMORY_USAGE.name()).get(0).getValue().get(1))
            : 0;
    }

    private double mapReceiveBandwidth(Map<String, List<PodMetricsResult>> metricsByName) {
        return metricsByName.containsKey(Metric.BANDWIDTH_RX.name())
            ? Double.parseDouble(metricsByName.get(Metric.BANDWIDTH_RX.name()).get(0).getValue().get(1))
            : 0.0;
    }

    private double mapTransmitBandwidth(Map<String, List<PodMetricsResult>> metricsByName) {
        return metricsByName.containsKey(Metric.BANDWIDTH_TX.name())
            ? Double.parseDouble(metricsByName.get(Metric.BANDWIDTH_TX.name()).get(0).getValue().get(1))
            : 0.0;
    }

    private Double mapCpuCoresLimit(Map<String, List<PodMetricsResult>> metricsByName) {
        return metricsByName.containsKey(ResourceLimit.CPU_CORES_LIMIT.name())
            ? Double.parseDouble(metricsByName.get(ResourceLimit.CPU_CORES_LIMIT.name()).get(0).getValue().get(1))
            : null;
    }

    private Long mapMemoryLimit(Map<String, List<PodMetricsResult>> metricsByName) {
        return metricsByName.containsKey(ResourceLimit.MEMORY_LIMIT.name())
            ? Long.parseLong(metricsByName.get(ResourceLimit.MEMORY_LIMIT.name()).get(0).getValue().get(1))
            : null;
    }

    private Instant mapTimestamp(List<PodMetricsResult> results) {
        return !results.isEmpty()
            ? Instant.ofEpochSecond((long) Double.parseDouble(results.get(0).getValue().get(0)))
            : Instant.ofEpochSecond(0);
    }
}
