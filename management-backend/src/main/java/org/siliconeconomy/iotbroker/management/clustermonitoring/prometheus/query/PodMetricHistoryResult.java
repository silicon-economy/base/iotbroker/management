/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * This DTO represents the result of a single metric's history (e.g. cpu usage or receive bandwidth) for a single pod
 * as received by the Prometheus HTTP API. The results of the queries provided by {@link PodMetricHistoryQuery} are
 * mapped to instances of this class.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PodMetricHistoryResult {

    /**
     * A description of the metric.
     */
    PodMetricDescription metric;
    /**
     * The actual metric values.
     * The list contains entries for each value and timestamp pair that represents a particular pod metric value at
     * some point in time. Each entry itself is a list that contains two elements:
     * <ul>
     * <li>First element: The unix timestamp (that is to be parsed as a double)</li>
     * <li>Second element: The actual value (parsing depends on the metric type)</li>
     * </ul>
     */
    List<List<String>> values;
}
