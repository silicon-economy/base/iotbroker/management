/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.mapper;

import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetricHistory;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetricHistoryValue;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.Metric;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.PodMetricHistoryQuery;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.PodMetricHistoryResult;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Used to map query results as received by the Prometheus HTTP API to {@link PodMetricHistory} instances.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public class PodMetricHistoryMapper {

    /**
     * Maps a {@link PodMetricHistoryResult} which contains the history of a specific metric for a single pod as
     * received by a {@link PodMetricHistoryQuery} to a {@link PodMetricHistory} instance.
     *
     * @param result The {@link PodMetricHistoryResult}.
     * @return The query result mapped to a {@link PodMetricHistory} instance.
     */
    public PodMetricHistory map(PodMetricHistoryResult result) {
        requireNonNull(result, "results");

        return new PodMetricHistory()
            .setName(result.getMetric().getPod())
            .setValues(mapValues(result));
    }

    private List<PodMetricHistoryValue> mapValues(PodMetricHistoryResult result) {
        if (Objects.equals(result.getMetric().getMetricName(), Metric.BANDWIDTH_RX.name())) {
            return mapDoubleValues(result);
        } else if (Objects.equals(result.getMetric().getMetricName(), Metric.BANDWIDTH_TX.name())) {
            return mapDoubleValues(result);
        } else if (Objects.equals(result.getMetric().getMetricName(), Metric.CPU_USAGE.name())) {
            return mapDoubleValues(result);
        } else if (Objects.equals(result.getMetric().getMetricName(), Metric.MEMORY_USAGE.name())) {
            return mapLongValues(result);
        }

        return new ArrayList<>();
    }

    private List<PodMetricHistoryValue> mapDoubleValues(PodMetricHistoryResult result) {
        return result.getValues().stream()
            .map(singleValue -> new PodMetricHistoryValue()
                .setTimestamp(mapTimestamp(singleValue.get(0)))
                .setValue(Double.parseDouble(singleValue.get(1)))
            )
            .collect(Collectors.toList());
    }

    private List<PodMetricHistoryValue> mapLongValues(PodMetricHistoryResult result) {
        return result.getValues().stream()
            .map(singleValue -> new PodMetricHistoryValue()
                .setTimestamp(mapTimestamp(singleValue.get(0)))
                .setValue(Long.parseLong(singleValue.get(1)))
            )
            .collect(Collectors.toList());
    }

    private Instant mapTimestamp(String timestampAsEpochSecond) {
        return Instant.ofEpochSecond((long) Double.parseDouble(timestampAsEpochSecond));
    }
}
