/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.AdapterInformation;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetricHistory;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetrics;
import org.siliconeconomy.iotbroker.management.clustermonitoring.mapper.AdapterInformationMapper;
import org.siliconeconomy.iotbroker.management.clustermonitoring.mapper.PodMetricHistoryMapper;
import org.siliconeconomy.iotbroker.management.clustermonitoring.mapper.PodMetricsMapper;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.PrometheusClient;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Provides methods for retrieving monitoring information about the IoT broker's cluster.
 * The service uses a {@link PrometheusClient} to send queries to the Prometheus HTTP API and maps the responses to
 * corresponding DTOs.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Service
public class ClusterMonitoringService {

    /**
     * A pointer to the JSON field containing the results contained in a Prometheus HTTP API response.
     */
    private static final JsonPointer RESULT = JsonPointer.valueOf("/data/result");
    /**
     * Used to access/query the Prometheus HTTP API.
     */
    private final PrometheusClient client;
    /**
     * Used to read the JSON responses received from the Prometheus HTTP API.
     */
    private final ObjectMapper mapper = new ObjectMapper();
    /**
     * Used to map responses that contain information about pod metrics to corresponding DTOs.
     */
    private final PodMetricsMapper podMetricsMapper = new PodMetricsMapper();
    /**
     * Used to map responses that contain information about running adapters to corresponding DTOs.
     */
    private final AdapterInformationMapper adapterInformationMapper = new AdapterInformationMapper();
    /**
     * Used to map responses that contain historical information about a specific pod metric to corresponding DTOs.
     */
    private final PodMetricHistoryMapper podMetricHistoryMapper = new PodMetricHistoryMapper();
    /**
     * Defines query strings for querying pod metrics.
     */
    private final PodMetricsQuery podMetricsQuery;
    /**
     * Defines query strings for querying historical values for a specific metric of a pod.
     */
    private final PodMetricHistoryQuery podMetricHistoryQuery;
    /**
     * Defines query strings for querying information about running adapters.
     */
    private final AdaptersQuery adaptersQuery;

    @Autowired
    public ClusterMonitoringService(PrometheusClient client,
                                    PodMetricsQuery podMetricsQuery,
                                    PodMetricHistoryQuery podMetricHistoryQuery,
                                    AdaptersQuery adaptersQuery) {
        this.client = client;
        this.podMetricsQuery = podMetricsQuery;
        this.podMetricHistoryQuery = podMetricHistoryQuery;
        this.adaptersQuery = adaptersQuery;
    }

    /**
     * Queries the Prometheus HTTP API to get a list of metrics for all pods running in the IoT broker's cluster.
     *
     * @return A list of {@link PodMetrics}.
     * @throws IOException If the JSON response received from the Prometheus HTTP API could not be parsed to JSON.
     */
    public List<PodMetrics> getPodMetrics() throws IOException {
        List<PodMetricsResult> results = new ArrayList<>();
        CollectionType collectionType
            = mapper.getTypeFactory().constructCollectionType(List.class, PodMetricsResult.class);

        results.addAll(
            mapper.readValue(queryPrometheus(podMetricsQuery.componentAllPods()), collectionType)
        );
        results.addAll(
            mapper.readValue(queryPrometheus(podMetricsQuery.infrastructureAllPods()), collectionType)
        );

        Map<String, List<PodMetricsResult>> resultsByPod
            = results.stream().collect(Collectors.groupingBy(result -> result.getMetric().getPod()));

        List<PodMetrics> podsMetrics = new ArrayList<>();
        resultsByPod.forEach((podName, resultList) -> podsMetrics.add(podMetricsMapper.map(podName, resultList)));

        return podsMetrics;
    }

    /**
     * Queries the Prometheus HTTP API to get the metrics for the pod with the given name running in the IoT
     * broker's cluster.
     *
     * @param name The name of the pod.
     * @return The {@link PodMetrics}.
     * @throws IOException If the JSON response received from the Prometheus HTTP API could not be parsed to JSON.
     */
    public PodMetrics getPodMetricsByPodName(String name) throws IOException {
        List<PodMetricsResult> results = new ArrayList<>();
        CollectionType collectionType
            = mapper.getTypeFactory().constructCollectionType(List.class, PodMetricsResult.class);

        results.addAll(
            mapper.readValue(queryPrometheus(podMetricsQuery.componentSinglePod(name)), collectionType)
        );
        results.addAll(
            mapper.readValue(queryPrometheus(podMetricsQuery.infrastructureSinglePod(name)), collectionType)
        );

        Map<String, List<PodMetricsResult>> resultsByPod
            = results.stream().collect(Collectors.groupingBy(result -> result.getMetric().getPod()));

        if (!resultsByPod.containsKey(name)) {
            throw new PodNotFoundException("A pod with name '" + name + "' could not be found.");
        }

        return podMetricsMapper.map(name, resultsByPod.get(name));
    }

    /**
     * Queries the Prometheus HTTP API to get information about adapters running in the IoT broker's cluster.
     *
     * @return A list of {@link AdapterInformation}.
     * @throws IOException If the JSON response received from the Prometheus HTTP API could not be parsed to JSON.
     */
    public List<AdapterInformation> getAdapterInformation() throws IOException {
        List<AdaptersResult> queryResults = mapper.readValue(
            queryPrometheus(adaptersQuery.adapterAllInstances()),
            mapper.getTypeFactory().constructCollectionType(List.class, AdaptersResult.class)
        );

        Map<String, List<AdaptersResult>> resultsByAdapterType
            = queryResults.stream().collect(Collectors.groupingBy(result -> result.getMetric().getAdapterType()));

        List<AdapterInformation> adapterInformation = new ArrayList<>();
        resultsByAdapterType.forEach(
            (adapterType, resultList) -> adapterInformation.add(adapterInformationMapper.map(resultList))
        );

        return adapterInformation;
    }

    /**
     * Queries the Prometheus HTTP API to get historical values of a single metric for the pod with the given name
     * running in the IoT broker's cluster.
     *
     * @param podName   The name of the pod.
     * @param metric    The metric to get historical values for.
     * @param range     The range (in seconds) for which values are to be retrieved. The range is calculated based on
     *                  the current time.
     * @param stepWidth The step width (i.e. resolution) the values should be retrieved with.
     * @return The {@link PodMetricHistory}.
     * @throws IOException If the JSON response received from the Prometheus HTTP API could not be parsed to JSON.
     */
    public PodMetricHistory getPodMetricHistory(String podName, Metric metric, int range, int stepWidth)
        throws IOException {
        List<PodMetricHistoryResult> queryResults = mapper.readValue(
            queryRangePrometheus(podMetricHistoryQuery.metricHistory(podName, metric), range, stepWidth),
            mapper.getTypeFactory().constructCollectionType(List.class, PodMetricHistoryResult.class)
        );

        if (queryResults.isEmpty()) {
            throw new PodNotFoundException("A pod with name '" + podName + "' could not be found.");
        }

        return podMetricHistoryMapper.map(queryResults.get(0));
    }

    /**
     * Queries the Prometheus HTTP API with the given query string and extracts the result portion of the received
     * response. The given query string is evaluated at a single point in time.
     *
     * @param queryString The query string to use.
     * @return The result portion of the received Prometheus response as a JSON string.
     * @throws IOException If there was an error querying the Prometheus HTTP API.
     */
    private String queryPrometheus(String queryString) throws IOException {
        String queryResponseString = client.query(queryString);

        return extractPrometheusResult(queryResponseString);
    }

    /**
     * Queries the Prometheus HTTP API with the given query string and extracts the result portion of the received
     * response. The given query string is evaluated over a range of time.
     *
     * @param queryString The query string to use.
     * @param range       The range (in seconds) for which values are to be evaluated. The range is calculated based on
     *                    the current time.
     * @param stepWidth   The step width (i.e. resolution) the values should be evaluated with.
     * @return The result portion of the received Prometheus response as a JSON string.
     * @throws IOException If there was an error querying the Prometheus HTTP API.
     */
    private String queryRangePrometheus(String queryString, int range, int stepWidth) throws IOException {
        Instant currentTime = Instant.now();
        String queryResponseString = client.queryRange(
            queryString,
            currentTime.minusSeconds(range).getEpochSecond(),
            currentTime.getEpochSecond(),
            stepWidth
        );

        return extractPrometheusResult(queryResponseString);
    }

    private String extractPrometheusResult(String prometheusResponse) throws IOException {
        JsonNode queryResponseNode = mapper.readTree(prometheusResponse);
        if (queryResponseNode == null) {
            throw new InvalidPrometheusResponeException("Invalid Prometheus HTTP API response. Empty response.");
        }

        JsonNode resultNode = queryResponseNode.at(RESULT);
        if (resultNode.isMissingNode()) {
            throw new InvalidPrometheusResponeException("Invalid Prometheus HTTP API response. No 'result' element in: "
                + prometheusResponse);
        }

        return resultNode.toString();
    }
}
