/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.Instant;
import java.util.List;

/**
 * This DTO is used to transfer information about running IoT broker adapters.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class AdapterInformation {

    /**
     * The type of adapter the information belongs to.
     */
    private String adapterType;
    /**
     * The number of adapter instances running (i.e. the number of pods).
     */
    private int instanceCount;
    /**
     * A list containing information about the actual adapter instances running.
     */
    private List<AdapterInstance> instances;

    /**
     * Represents a single adapter instance.
     */
    @Getter
    @Setter
    @NoArgsConstructor
    public static class AdapterInstance {

        /**
         * The name of the adapter instance (i.e. the names of the pods).
         */
        private String instanceName;
        /**
         * The point in time at which the adapter instance was started.
         */
        private Instant timeStarted;
    }
}
