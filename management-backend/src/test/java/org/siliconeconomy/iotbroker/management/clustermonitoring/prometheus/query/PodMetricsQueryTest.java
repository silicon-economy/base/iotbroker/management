/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest(
    classes = {PodMetricsQuery.class},
    properties = {
        "cluster.monitoring.pod.label.infrastructure=label_app_kubernetes_io_name",
        "cluster.monitoring.pod.label.component=label_sele_service",
        "cluster.monitoring.namespace=iot-broker"
    }
)
class PodMetricsQueryTest {

    @Autowired
    @Qualifier("webApplicationContext")
    private ResourceLoader resourceLoader;

    @Autowired
    private PodMetricsQuery query;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testInfrastructureAllPods() throws IOException {
        String expected = removeLineBreaksAndDuplicateWhitespace(loadResourceAsString(
            "testdata/PodMetricsQueryTest_prometheusRequest_infrastructureAllPods.txt"
        ));
        String result = removeLineBreaksAndDuplicateWhitespace(query.infrastructureAllPods());
        assertThat(result, is(expected));
    }

    @Test
    void testInfrastructureSinglePod() throws IOException {
        String expected = removeLineBreaksAndDuplicateWhitespace(loadResourceAsString(
            "testdata/PodMetricsQueryTest_prometheusRequest_infrastructureSinglePod.txt"
        ));
        String result = removeLineBreaksAndDuplicateWhitespace(query.infrastructureSinglePod("pod-name"));
        assertThat(result, is(expected));
    }

    @Test
    void testComponentAllPods() throws IOException {
        String expected = removeLineBreaksAndDuplicateWhitespace(loadResourceAsString(
            "testdata/PodMetricsQueryTest_prometheusRequest_componentAllPods.txt"
        ));
        String result = removeLineBreaksAndDuplicateWhitespace(query.componentAllPods());
        assertThat(result, is(expected));
    }

    @Test
    void testComponentSinglePod() throws IOException {
        String expected = removeLineBreaksAndDuplicateWhitespace(loadResourceAsString(
            "testdata/PodMetricsQueryTest_prometheusRequest_componentSinglePod.txt"
        ));
        String result = removeLineBreaksAndDuplicateWhitespace(query.componentSinglePod("pod-name"));
        assertThat(result, is(expected));
    }

    private String removeLineBreaksAndDuplicateWhitespace(String source) {
        return source
            .trim()
            .replaceAll("\n", "")
            .replaceAll(" +", " ");
    }

    private String loadResourceAsString(String resoucePath) throws IOException {
        return new String(
            Files.readAllBytes(resourceLoader.getResource("classpath:" + resoucePath).getFile().toPath())
        );
    }
}
