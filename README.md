> This repository is no longer maintained.
> The IoT Broker and its core components (including the components in this repository) are now maintained in a single repository at: https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker/iotbroker

# IoT Broker Management application

The IoT Broker Management application provides an interface to manage the IoT Broker and devices connected to it.
It is composed of two components: A backend and a frontend.
The backend provides the information and interfaces required by the frontend.
The frontend displays the information provided by the backend and is intended for administrative purposes.

## Documentation

For more information on the components in this project, please refer to the IoT Broker's arc42 documentation maintained in the IoT Broker core project.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` files of the corresponding components.
