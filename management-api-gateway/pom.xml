<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.siliconeconomy.iotbroker</groupId>
    <artifactId>management-api-gateway</artifactId>
    <version>0.10.3</version>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.6.7</version>
    </parent>

    <licenses>
        <license>
            <name>Open Logistics License, Version 1.0</name>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <!-- Project configuration -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- Set the Java version for the maven compiler plugin -->
        <java.version>11</java.version>
    </properties>

    <dependencies>
        <!-- Spring dependencies -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-gateway</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-log4j2</artifactId>
        </dependency>

        <!-- Other dependencies -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.22</version>
            <scope>provided</scope>
        </dependency>

        <!-- Testing dependencies -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>2021.0.2</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <repositories>
        <repository>
            <id>selemaven</id>
            <name>SELE maven artifacts and caching repo</name>
            <url>https://nexus.apps.sele.iml.fraunhofer.de/repository/selemaven/</url>
            <layout>default</layout>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <uniqueVersion>false</uniqueVersion>
            <id>selemaven-release</id>
            <name>SELE maven release artifacts</name>
            <url>https://nexus.apps.sele.iml.fraunhofer.de/repository/selemaven-release/</url>
            <layout>default</layout>
        </repository>
        <snapshotRepository>
            <uniqueVersion>true</uniqueVersion>
            <id>selemaven-snapshot</id>
            <name>SELE maven snapshot artifacts</name>
            <url>https://nexus.apps.sele.iml.fraunhofer.de/repository/selemaven-snapshot/</url>
            <layout>default</layout>
        </snapshotRepository>
    </distributionManagement>

    <build>
        <plugins>
            <!-- Spring Boot plugins -->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <!-- Test coverage report plugins -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <!-- Don't use the latest version, but the one that is compatible with Spring Boot. -->
                <version>2.22.2</version>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.5</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Plugin for generating third party licenses reports -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <version>2.0.0</version>
            </plugin>
        </plugins>
    </build>

</project>
