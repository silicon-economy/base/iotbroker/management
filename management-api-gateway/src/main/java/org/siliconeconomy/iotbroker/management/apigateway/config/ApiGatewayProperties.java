/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.apigateway.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;

/**
 * Definition of configurable application properties.
 *
 * @author M. Grzenia
 */
@AllArgsConstructor
@Getter
@ConfigurationProperties(prefix = "api.gateway")
@ConstructorBinding
public class ApiGatewayProperties {

  /**
   * The URL for accessing the Device Registry Service.
   */
  private final String deviceRegistryServiceUrl;
  /**
   * The URL for accessing the Sensor Data History Service.
   */
  private final String sensorDataHistoryServiceUrl;
  /**
   * The URL for accessing the Sensor Data Subscription Service's WebSocket interface.
   */
  private final String sensorDataSubscriptionServiceWebsocketUrl;
  /**
   * The list of origins for which cross-origin requests are allowed.
   */
  private final List<String> corsAllowedOrigins;
}
