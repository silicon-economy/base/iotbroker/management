/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.apigateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.factory.DedupeResponseHeaderGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.GatewayFilterSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import java.util.List;

/**
 * Configuration of Spring Cloud Gateway.
 *
 * @author M. Grzenia
 */
@Configuration
public class ApiGatewayConfig {

  /**
   * Defines configurable application properties.
   */
  private final ApiGatewayProperties properties;

  @Autowired
  public ApiGatewayConfig(ApiGatewayProperties properties) {
    this.properties = properties;
  }

  /**
   * Configures the routes for forwarding requests.
   * <p>
   * Depending on the request path, requests are forwarded to one of the following services:
   * <ul>
   *   <li>Device Registry Service</li>
   *   <li>Sensor Data History Service</li>
   *   <li>Sensor Data Subscription Service</li>
   * </ul>
   * <p>
   * Note that filters are applied to some routes
   * (see {@link #dedupeAccessControlAllowedOriginHeader(GatewayFilterSpec)}) due to the
   * (unfortunately unavoidable) CORS configuration (see {@link #corsWebFilter()}).
   *
   * @param builder The builder to use to configure the routes.
   * @return The {@link RouteLocator} containing the configured routes.
   */
  @Bean
  public RouteLocator apiGatewayRouteLocator(RouteLocatorBuilder builder) {
    return builder.routes()
        .route("device-registry-service-route",
               predicateSpec -> predicateSpec
                   .path("/deviceInstances/**", "/deviceTypes/**")
                   .filters(this::dedupeAccessControlAllowedOriginHeader)
                   .uri(properties.getDeviceRegistryServiceUrl())
        )
        .route("sensor-data-history-service-route",
               predicateSpec -> predicateSpec
                   .path("/messages/**")
                   .filters(this::dedupeAccessControlAllowedOriginHeader)
                   .uri(properties.getSensorDataHistoryServiceUrl())
        )
        .route("sensor-data-subscription-service-route",
               predicateSpec -> predicateSpec
                   .path("/websocket")
                   .uri(properties.getSensorDataSubscriptionServiceWebsocketUrl())
        )
        .build();
  }

  /**
   * Configures CORS for Spring Cloud Gateway (or rather Spring WebFlux, which Spring Cloud Gateway
   * is built on).
   * <p>
   * Spring Cloud Gateway can be configured to control CORS behavior so that developers have the
   * option to handle it in the gateway itself rather than in the downstream services. However, by
   * default, CORS is disabled in Spring Cloud Gateway and it is up to the downstream services to
   * handle CORS requests.
   * <p>
   * Ideally, in our use case, we would not want to handle CORS in the gateway and simply forward
   * all requests to the downstream services. Therefore, Spring Cloud Gateway's default behaviour
   * would certainly meet our requirements. However, things get complicated once preflight requests
   * come into play. For more information on preflight requests, see e.g.
   * <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS">CORS @ MDN</a>.
   * <p>
   * Spring Cloud Gateway is built on Spring WebFlux, and in Spring WebFlux, preflight requests are
   * handled directly, which ultimately results in them not being forwarded to the downstream
   * services (see
   * <a href="https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html#webflux-cors-processing">
   * Spring WebFlux documentation</a>). Without a CORS configuration (which, as mentioned above,
   * Spring Cloud Gateway does not come with by default), the default behavior of Spring WebFlux is
   * to reject preflight requests. As of now, there is no easy way to change this default behaviour
   * other than configuring CORS for the gateway as well. It should be noted that preflight requests
   * are still not forwarded to the downstream services, despite an existing CORS configuration.
   * However, this is acceptable for our use case, since "regular" requests are forwarded without
   * any problems.
   * <p>
   * Note: Since Spring Cloud Gateway uses WebFlux functional endpoints, we need to define a
   * {@link CorsWebFilter} and cannot use {@link WebFluxConfigurer}.
   *
   * @return The {@link CorsWebFilter} to use with the gateway.
   * @see #dedupeAccessControlAllowedOriginHeader(GatewayFilterSpec)
   */
  @Bean
  public CorsWebFilter corsWebFilter() {
    var corsConfiguration = new CorsConfiguration();
    corsConfiguration.setAllowedOrigins(properties.getCorsAllowedOrigins());
    corsConfiguration.setAllowedMethods(List.of(CorsConfiguration.ALL));
    corsConfiguration.setAllowedHeaders(List.of(CorsConfiguration.ALL));

    var corsConfigurationSource = new UrlBasedCorsConfigurationSource();
    corsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
    return new CorsWebFilter(corsConfigurationSource);
  }

  /**
   * Configures a {@link GatewayFilterSpec} to dedupe CORS-specific response headers.
   * <p>
   * Since CORS <em>has</em> to be configured for this gateway (see {@link #corsWebFilter()}) and
   * CORS handling is also performed in the downstream services, responses from this gateway to a
   * client end up with duplicate CORS headers (one added by the gateway and one added be the
   * downstream service). Responses with duplicate CORS headers are not handled by browsers, which
   * is why we have to remove duplications.
   * <p>
   * Note: Spring Cloud Gateway allows <em>default filters</em> to be configured that apply to all
   * routes. These would be perfect for such a use case. Unfortunately, as of now, configuration of
   * default filters is only possible via properties configuration and not via Java configuration.
   *
   * @param gatewayFilterSpec The {@link GatewayFilterSpec} to configure.
   * @return The configured {@link GatewayFilterSpec}.
   */
  private GatewayFilterSpec dedupeAccessControlAllowedOriginHeader(
      GatewayFilterSpec gatewayFilterSpec) {
    // When the response contains more than one "Access-Control-Allow-Origin" header, the last one
    // originates from the downstream service. Keep this one, as it is the only one that matters to
    // us and ultimately to the client (i.e. the frontend).
    return gatewayFilterSpec.dedupeResponseHeader(
        HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN,
        DedupeResponseHeaderGatewayFilterFactory.Strategy.RETAIN_LAST.name()
    );
  }
}
