/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.management.apigateway;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for the application.
 * <p>
 * This application merely configures Spring Cloud Gateway and there is no business logic to test.
 * Therefore, we simply test if the application context loads properly at application startup
 * (assuming that the required properties are set).
 * <p>
 * A unit test doesn't make sense, since it would rather test the functionality of
 * Spring Cloud Gateway itself and there's not really a point in testing Spring Cloud Gateway
 * features. On the other hand, an integration test with the real services would make sense here.
 * However, this is rather difficult to implement.
 *
 * @author M. Grzenia
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties = {
        "api.gateway.deviceRegistryServiceUrl=http://registryservicehost:8080",
        "api.gateway.sensorDataHistoryServiceUrl=http://sensordatahistoryservicehost:8080",
        "api.gateway.sensorDataSubscriptionServiceWebsocketUrl=ws://sensordatasubscriptionservicehost:8080/websocket",
        "api.gateway.corsAllowedOrigins=*"
    }
)
class ManagementApiGatewayApplicationTest {

  @Autowired
  private ApplicationContext applicationContext;

  @Test
  void contextLoads() {
    assertThat(applicationContext).isNotNull();
  }
}
