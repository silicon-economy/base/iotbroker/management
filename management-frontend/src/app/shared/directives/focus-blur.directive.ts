/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: 'a, button'
})
export class FocusBlurDirective {
  constructor(private element: ElementRef) {}

  @HostListener('click')
  onClick() {
    this.element.nativeElement.blur();
  }
}
