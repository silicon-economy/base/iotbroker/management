/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { FormControlPipe } from 'src/app/shared/pipes/form-control.pipe';

describe('FormControlPipe', () => {
  it('create an instance', () => {
    const pipe = new FormControlPipe();
    expect(pipe).toBeTruthy();
  });
});
