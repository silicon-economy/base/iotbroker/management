/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TableColumnConfiguration } from 'src/app/shared/components/expandable-table/models/table-column-configuration.interface';

@Injectable({
  providedIn: 'root'
})
export class TableConfigService {

  constructor(public translate: TranslateService) {
  }

  initialiseTableColumnConfig(dataAttributesToDisplay: string[], i18nPrefix: string): TableColumnConfiguration[] {
    const items: TableColumnConfiguration[] = [];
    for (const name of dataAttributesToDisplay) {
      items.push(this.createTableColumnConfigElement(name, i18nPrefix));
    }
    return items;
  }

  createTableColumnConfigElement(name: string, i18nPrefix: string) {
    const translateService = this.translate;
    return {
      dataAttributeToDisplay: name,
      header(): string {
        return translateService.instant(i18nPrefix + name);
      }
    };
  }
}
