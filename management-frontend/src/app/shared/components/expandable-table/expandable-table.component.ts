/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { AfterContentInit, AfterViewInit, Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TableColumnConfiguration } from 'src/app/shared/components/expandable-table/models/table-column-configuration.interface';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';

@Component({
  selector: 'app-expandable-table',
  templateUrl: './expandable-table.component.html',
  styleUrls: ['./expandable-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ExpandableTableComponent<T> implements AfterContentInit, AfterViewInit, OnInit {

  @ViewChild(MatSort) sort: MatSort;

  @Input() itemTemplate: TemplateRef<object>;
  @Input() description: string;
  @Input() tableColumnConfiguration: TableColumnConfiguration[] = [];

  // Variables for the table
  public dataSource = new MatTableDataSource<T>();
  public expandedElement: T | null;

  public actionColumn: TableColumnConfiguration;

  constructor(private tableService: TableConfigService) {

  }

  public ngOnInit() {
    this.actionColumn = this.tableService.createTableColumnConfigElement('actions', 'expandabletable.');
  }

  public ngAfterContentInit(): void {
    this.dataSource.data = [];
  }

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public getAttributes(): string[] {
    const attributes = this.tableColumnConfiguration.map((element) => element.dataAttributeToDisplay);
    attributes.push(this.actionColumn.dataAttributeToDisplay);
    return attributes;
  }

  public addElement(element: T): void {
    this.dataSource.data.push(element);
    this.dataSource._updateChangeSubscription();
  }

  public addElementToBeginning(element: T): void {
    this.dataSource.data.unshift(element);
    this.dataSource._updateChangeSubscription();
  }

  public addElements(elements: T[]): void {
    for (const element of elements) {
      this.dataSource.data.push(element);
    }
    this.dataSource._updateChangeSubscription();
  }

  public clearElements(): void {
    this.dataSource.data = [];
    this.dataSource._updateChangeSubscription();
  }


}
