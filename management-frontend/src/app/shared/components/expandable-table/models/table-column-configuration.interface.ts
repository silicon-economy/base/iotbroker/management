/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

export interface TableColumnConfiguration {
  dataAttributeToDisplay: string;
  header(): string;
}
