/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { MatTableModule } from '@angular/material/table';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatIconModule } from '@angular/material/icon';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('ExpandableTableComponent', () => {
  let component: ExpandableTableComponent<unknown>;
  let fixture: ComponentFixture<ExpandableTableComponent<unknown>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExpandableTableComponent],
      imports: [
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        MatTableModule,
        HttpClientTestingModule,
        MatIconModule
      ]
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandableTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add one message', () => {
    component.addElement({});
    expect(component.dataSource.data.length).toEqual(1);
  });

  it('should add multiple messages', () => {
    component.dataSource.data = [];
    component.addElements([{}, {}]);
    expect(component.dataSource.data.length).toEqual(2);
  });

  it('should delete the messages', () => {
    component.addElement({});
    component.clearElements();
    expect(component.dataSource.data.length).toEqual(0);
  });

  it('should add a message in the beginning', () => {
    component.clearElements();
    component.addElement({ 'count': 1 });
    component.addElementToBeginning({ 'count': 2 });
    expect(component.dataSource.data.length).toEqual(2);
    expect(component.dataSource.data[0]).toEqual({ 'count': 2 });
  });
});
