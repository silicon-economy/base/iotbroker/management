/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-autofill-select',
  templateUrl: './autofill-select.component.html',
  styleUrls: ['./autofill-select.component.scss']
})
export class AutofillSelectComponent {

  @Input() label = '';
  @Input() error = this.label + ' is required';
  @Input() allowWildcards: boolean;

  element = new FormControl('', [Validators.required]);
  elementList: string[] = [];
  filteredElements: Observable<string[]>;

  /**
   * adds a * to the start of the list
   * whenever the value of {@link element} changes {@link elementList} is filtered
   * @param list The list of elements to select from
   */
  initializeElements(list: string[]) {
    this.elementList = list.slice();
    if (this.allowWildcards) {
      this.elementList.unshift('*');
    }

    this.filteredElements = this.element.valueChanges.pipe(
        startWith(''),
        map(value => this._filterValues(value, this.elementList)),
    );
  }

  /**
   * Filters {@link elementList} with a given value
   * @param value The Value the list is filtered with
   * @param values The list to filter
   */
  public _filterValues(value: string, values: string[]): string[] {
    const filterValue = value.toLowerCase();
    return values.filter(option => {
      if (option === '*') {
        return true;
      }
      return option.toLowerCase().includes(filterValue);
    });
  }

}
