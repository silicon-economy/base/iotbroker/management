/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Component, Input } from '@angular/core';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-autofill-select',
  template: '',
  providers: [
    {
      provide: AutofillSelectComponent,
      useClass: AutofillSelectStubComponent
    }
  ]
})
export class AutofillSelectStubComponent {

  @Input() label = '';
  @Input() error = this.label + ' is required';
  @Input() allowWildcards: boolean;

  elementList: string[] = [];
  element = new FormControl('', [Validators.required]);



  initializeElements(list: string[]) {
    this.elementList = list;
  }
}
