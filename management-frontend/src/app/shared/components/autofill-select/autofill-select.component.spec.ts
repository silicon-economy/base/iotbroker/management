/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';

describe('AutofillSelectComponent', () => {
  let component: AutofillSelectComponent;
  let fixture: ComponentFixture<AutofillSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatAutocompleteModule,
        MatSelectModule,
      ],
      declarations: [ AutofillSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutofillSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create the list correctly with wildcards', () => {
    component.allowWildcards = true;
    const list = ['1', '2', '3'];
    const resultList = ['*', '1', '2', '3'];
    component.initializeElements(list);
    expect(component.elementList).toEqual(resultList);
  });

  it('should create the list correctly without wildcards', () => {
    component.allowWildcards = false;
    const list = ['1', '2', '3'];
    const resultList = ['1', '2', '3'];
    component.initializeElements(list);
    expect(component.elementList).toEqual(resultList);
  });

  it('should filter correctly', () => {
    const list = ['1a', '2a', '3'];
    const resultList = ['1a', '2a'];
    const result = component._filterValues('a', list);
    expect(result).toEqual(resultList);
  });
});
