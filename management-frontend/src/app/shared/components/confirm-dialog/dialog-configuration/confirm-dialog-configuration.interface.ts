/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Type } from '@angular/core';
import { Subject } from 'rxjs';

export interface ConfirmDialogConfiguration<C> {
  contentComponentType: Type<C>;

  configureContentComponent(contentComponent: C): void;

  dialogConfirmAction(contentComponent: C): Subject<boolean>;
}
