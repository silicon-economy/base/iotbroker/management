/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { ConfirmDialogConfiguration } from 'src/app/shared/components/confirm-dialog/dialog-configuration/confirm-dialog-configuration.interface';

export interface DialogData<C> {
  title: string,
  componentConfiguration: ConfirmDialogConfiguration<C>;
}
