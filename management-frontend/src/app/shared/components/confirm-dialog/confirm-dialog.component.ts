/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from 'src/app/shared/components/confirm-dialog/models/dialog-data.interface';
import { DialogContentDirective } from 'src/app/shared/components/confirm-dialog/directives/dialog-content.directive';
import { ConfirmDialogConfiguration } from 'src/app/shared/components/confirm-dialog/dialog-configuration/confirm-dialog-configuration.interface';

@Component({
  selector: 'app-device-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent<C> implements OnInit {

  @ViewChild(DialogContentDirective, { static: true }) dialogContentDirective: DialogContentDirective;
  dialogContent: C;
  componentConfiguration: ConfirmDialogConfiguration<C>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData<C>,
              public dialogRef: MatDialogRef<ConfirmDialogComponent<C>>) {
  }

  ngOnInit(): void {
    this.componentConfiguration = this.data.componentConfiguration;
    const componentRef = this.dialogContentDirective.viewContainerRef.createComponent<C>(this.componentConfiguration.contentComponentType);
    this.dialogContent = componentRef.instance;
    this.componentConfiguration.configureContentComponent(this.dialogContent);
  }

  /**
   * Performs the configured confirm action.
   * Closes the dialog if the action was performed successfully.
   */
  confirm() {
    this.componentConfiguration.dialogConfirmAction(this.dialogContent).subscribe(actionSuccessful => {
      if (actionSuccessful === true) {
        this.dialogRef.close();
      }
    });
  }

  /**
   * Closes the dialog.
   */
  cancel() {
    this.dialogRef.close(false);
  }
}
