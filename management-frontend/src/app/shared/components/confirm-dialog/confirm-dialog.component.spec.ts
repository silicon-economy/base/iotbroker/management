/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatInputModule } from '@angular/material/input';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { HttpLoaderFactory } from 'src/app/app.module';
import { DialogContentDirective } from 'src/app/shared/components/confirm-dialog/directives/dialog-content.directive';
import { DialogData } from 'src/app/shared/components/confirm-dialog/models/dialog-data.interface';
import { MatButton } from '@angular/material/button';

describe('ConfirmDialogComponent', () => {
  let component: ConfirmDialogComponent<MatButton>;
  let fixture: ComponentFixture<ConfirmDialogComponent<MatButton>>;

  const spy = jasmine.createSpyObj('MatDialogRef', ['close']);

  let mockDialogRef;

  const testData: DialogData<MatButton> = {
    title: '',
    componentConfiguration: {
      contentComponentType: MatButton,
      configureContentComponent(contentComponent: MatButton) {
        // Do nothing
      },
      dialogConfirmAction(): Subject<boolean> {
        const testSubject = new Subject<boolean>();
        testSubject.next(true);
        return testSubject;
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmDialogComponent, DialogContentDirective],
      imports: [
        MatCardModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatCheckboxModule,
        MatCardModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: testData },
        { provide: MatDialogRef, useValue: spy }
      ]
    });

    mockDialogRef = TestBed.inject(MatDialogRef);

    TestBed.compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent<ConfirmDialogComponent<MatButton>>(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit', () => {
    const testSubject = new Subject<boolean>();
    testSubject.next(true);
    spyOn(component.componentConfiguration, 'dialogConfirmAction').and.returnValue(testSubject);
    component.confirm();
    expect(component.componentConfiguration.dialogConfirmAction).toHaveBeenCalled();
  });

  it('should exit', () => {
    component.cancel();
    expect(mockDialogRef.close).toHaveBeenCalled();
  });

});
