/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDialogContent]'
})
export class DialogContentDirective {

  constructor(public viewContainerRef: ViewContainerRef) {
  }
}
