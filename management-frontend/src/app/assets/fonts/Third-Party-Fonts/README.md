## Third-party fonts which are used

Throughout the Application the Material Font "Roboto" is used as the default font and is included in the ttf-format.

> ### 'Roboto-Regular.ttf'
`Roboto-Regular.ttf`

> https://fonts.google.com/download?family=Roboto
