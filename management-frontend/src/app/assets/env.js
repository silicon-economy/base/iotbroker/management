/**
 * Loads environment variables.
 * Needs to be done in assets folder since environment.ts is barely accessible after build.
 * Attention: New environment variables also need to be added to assets/env.template.js
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

(function(window){
    window["env"]=window["env"]||{}
    window['env']['production'] = false;
    window["env"]["MANAGEMENT_BACKEND_URL"] = 'http://localhost:8080';
    window["env"]["MANAGEMENT_BACKEND_HOST"] = 'http://localhost';
    window["env"]["MANAGEMENT_BACKEND_PORT"] = "8080";
    window["env"]["DEVICE_REGISTRY_SERVICE_URL"] = 'http://localhost:8081';
    window["env"]["SENSOR_DATA_HISTORY_SERVICE_URL"] = 'http://localhost:8081';
    window["env"]["SENSOR_DATA_SUBSCRIPTION_SERVICE_WEBSOCKET_URL"] = 'ws://127.0.0.1:8081/websocket';
}(this));
