/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTreeModule } from '@angular/material/tree';
import { MatSliderModule } from '@angular/material/slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PrivacyComponent } from 'src/app/features/privacy/privacy.component';
import { ThemeService } from 'src/app/core/services/theme.service';
import { SidenavService } from 'src/app/core/services/sidenav.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LoadPageComponent } from 'src/app/features/load-page/load-page.component';
import { DeviceInstanceOverviewComponent } from 'src/app/features/devices/pages/device-instance-overview/device-instance-overview.component';
import { AdapterOverviewComponent } from 'src/app/features/adapter-overview/adapter-overview.component';
import { LiveMessageDisplayComponent } from 'src/app/features/device-messages/pages/live-message-display/live-message-display.component';
import { WelcomePageComponent } from 'src/app/features/welcome-page/welcome-page.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgApexchartsModule } from 'ng-apexcharts';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppComponent } from 'src/app/app.component';
import { FocusBlurDirective } from 'src/app/shared/directives/focus-blur.directive';
import { ConfirmationComponent } from 'src/app/shared/components/confirmation/confirmation.component';
import { CustomSpinnerComponent } from 'src/app/core/services/overlay.service';
import { DeviceTypeOverviewComponent } from 'src/app/features/devices/pages/device-type-overview/device-type-overview.component';
import { RxStompService } from 'src/app/features/device-messages/services/stomp/rx-stomp.service';
import { rxStompServiceFactory } from 'src/app/features/device-messages/services/stomp/rx-stomp-service-factory';
import { TopicSubscriptionComponent } from 'src/app/features/device-messages/components/topic-subscription/topic-subscription.component';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';
import { HistoryMessageDisplayComponent } from 'src/app/features/device-messages/pages/history-message-display/history-message-display.component';
import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { DeviceInstanceSelectionComponent } from 'src/app/features/device-messages/components/device-instance-selection/device-instance-selection.component';
import { WelcomePageEnglishComponent } from 'src/app/features/welcome-page/components/english/welcome-page-english.component';
import { WelcomePageGermanComponent } from 'src/app/features/welcome-page/components/german/welcome-page-german.component';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { DeviceInstanceEditFormComponent } from 'src/app/features/devices/components/device-instance-edit-form/device-instance-edit-form.component';
import { DeviceTypeEditFormComponent } from 'src/app/features/devices/components/device-type-edit-form/device-type-edit-form.component';
import { FormControlPipe } from 'src/app/shared/pipes/form-control.pipe';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { DialogContentDirective } from 'src/app/shared/components/confirm-dialog/directives/dialog-content.directive';
import { PaginatorIntl } from 'src/app/core/utils/paginator-intl/paginator-intl';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../app/assets/lang/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    FocusBlurDirective,
    PrivacyComponent,
    LoadPageComponent,
    DeviceInstanceOverviewComponent,
    AdapterOverviewComponent,
    LiveMessageDisplayComponent,
    WelcomePageComponent,
    ConfirmationComponent,
    CustomSpinnerComponent,
    DeviceTypeOverviewComponent,
    TopicSubscriptionComponent,
    HistoryMessageDisplayComponent,
    MessagesTableComponent,
    AutofillSelectComponent,
    DeviceInstanceSelectionComponent,
    WelcomePageEnglishComponent,
    WelcomePageGermanComponent,
    DeviceInstanceSelectionComponent,
    ExpandableTableComponent,
    DeviceInstanceEditFormComponent,
    DeviceTypeEditFormComponent,
    FormControlPipe,
    ConfirmDialogComponent,
    DialogContentDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FlexLayoutModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatTreeModule,
    MatSliderModule,
    MatCardModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    MatGridListModule,
    MatDialogModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatRadioModule,
    DragDropModule,
    MatSortModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    HttpClientModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatTooltipModule
  ],
  providers: [
    ThemeService,
    SidenavService,
    HttpClientModule,
    DeviceTypeService,
    DeviceInstanceService,
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
    },
    {
      provide: MatPaginatorIntl,
      useClass: PaginatorIntl
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
