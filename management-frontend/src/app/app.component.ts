/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDrawerMode } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { ThemeService } from 'src/app/core/services/theme.service';
import { SidenavService } from 'src/app/core/services/sidenav.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  panelOpenState = false;

  breakpointStateLtMd = false;
  themeModeIcon: string;
  sidenavMode: MatDrawerMode = 'side';
  sidenavState = true;

  constructor(
      public breakpointObserver: BreakpointObserver,
      public dialog: MatDialog,
      public themeService: ThemeService,
      public sidenavService: SidenavService,
      public translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.addLangs(['de']);
    translate.use('en');

    breakpointObserver.observe([
      Breakpoints.Small,
      Breakpoints.XSmall
    ]).subscribe(result => {
      if (result.matches) {
        this.activateHandsetLayout();
        this.breakpointStateLtMd = true;
      } else {
        this.deactivateHandsetLayout();
        this.breakpointStateLtMd = false;
      }
    });
  }

  ngOnInit(): void {
    // reset application state to default
    localStorage.clear();

    this.themeService.resetMode();
    this.themeService.resetTheme();
    document.body.classList.add(this.themeService.getAvailableThemes()[0].name + '-' +
        this.themeService.getAvailableModes()[0].name);
    this.themeModeIcon = this.themeService.getNextModeIcon();

    this.sidenavService.resetSidenavState();

  }

  private activateHandsetLayout() {
    this.sidenavMode = 'over';
    this.sidenavState = false;
  }

  private deactivateHandsetLayout() {
    this.sidenavMode = 'side';
    this.sidenavState = this.sidenavService.getSidenavState();
  }

  sidenavStateToggle() {
    if (!this.breakpointStateLtMd) {
      this.sidenavService.toggleSidenavState();
    }
  }

  changeLang(lang: string) {
    this.translate.use(lang);
  }

  CycleTheme(): void {
    const nextClassname = this.themeService.getThemeStr() + '-' + this.themeService.getNextModeObj().name;
    const currentClassname = this.themeService.getThemeStr() + '-' + this.themeService.getModeObj().name;
    if (nextClassname !== currentClassname) {
      document.body.classList.add(nextClassname);
      document.body.classList.remove(currentClassname);
      this.themeService.cycleMode();
      this.themeModeIcon = this.themeService.getNextModeIcon();
    }
  }
}
