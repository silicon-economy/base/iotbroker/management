/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: window['env'] && window['env']['production'] ? window['env']['production'] : false,
  // API_HOST has to include the protocol since we can not rely
  // on servers redirecting us to https when hardcoding http
  managementBackendUrl: window['env'] && window['env']['MANAGEMENT_BACKEND_URL'] ? window['env']['MANAGEMENT_BACKEND_URL'] : 'http://localhost:8080',
  managementBackendHost: window['env'] && window['env']['MANAGEMENT_BACKEND_HOST'] ? window['env']['MANAGEMENT_BACKEND_HOST'] : 'http://localhost',
  managementBackendPort: window['env'] && window['env']['MANAGEMENT_BACKEND_PORT'] ? window['env']['MANAGEMENT_BACKEND_PORT'] : '8080',
  deviceRegistryServiceUrl: window['env'] && window['env']['DEVICE_REGISTRY_SERVICE_URL'] ? window['env']['DEVICE_REGISTRY_SERVICE_URL'] : 'http://localhost:8081',
  sensorDataHistoryServiceUrl: window['env'] && window['env']['SENSOR_DATA_HISTORY_SERVICE_URL'] ? window['env']['SENSOR_DATA_HISTORY_SERVICE_URL'] : 'http://localhost:8081',
  sensorDataSubscriptionServiceWebsocketUrl: window['env'] && window['env']['SENSOR_DATA_SUBSCRIPTION_SERVICE_WEBSOCKET_URL'] ? window['env']['SENSOR_DATA_SUBSCRIPTION_SERVICE_WEBSOCKET_URL'] : 'ws://127.0.0.1:8081/websocket',

  // URLs and topics for httpService
  devicesUrl: '/devices',
  monitoringUrl: '/cluster/monitoring/pods',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
