/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { FormControl, ValidationErrors } from '@angular/forms';

/*
 * Validator to check if a string consists of only whitespace or is empty.
 */
export function isNotEffectivelyEmptyValidator(control: FormControl): ValidationErrors | null {
    const isValid = (control.value || '').trim().length > 0;
    return isValid ? null : { 'emptyOrOnlyWhitespace': true};
}
