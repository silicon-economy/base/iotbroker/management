/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Metrics } from 'src/app/core/models/metrics';

export interface Pod {
  name: string;
  label: string;
  values: Metrics;
}
