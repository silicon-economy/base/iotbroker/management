/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { AdapterInstance } from 'src/app/core/models/adapter-instance';

export class Adapter {
  adapterType: string;
  instanceCount: number;
  instances: AdapterInstance[];
}
