/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Datastream } from 'src/app/core/models/sensor-data/datastream';
import { Location } from 'src/app/core/models/sensor-data/location';

export class SensorDataMessage {
  id: string;
  originId: string;
  source: string;
  tenant: string;
  timestamp: string;
  location: Location;
  datastreams: Datastream[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
