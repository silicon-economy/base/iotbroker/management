/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { MapCode } from 'src/app/core/models/sensor-data/map-code';

export class MapCodeDetails {
  mapcode: MapCode;
}
