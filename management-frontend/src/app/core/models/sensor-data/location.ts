/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { LatitudeLongitudeDetails } from 'src/app/core/models/sensor-data/latitude-longitude-details';
import { MapCodeDetails } from 'src/app/core/models/sensor-data/map-code-details';
import { SimpleDetails } from 'src/app/core/models/sensor-data/simple-details';
import { LocationEncodingType } from 'src/app/core/models/sensor-data/location-encoding-type.enum';

export class Location {
  name: string;
  description: string;
  encodingType: LocationEncodingType;
  details: LatitudeLongitudeDetails | MapCodeDetails | SimpleDetails;
}
