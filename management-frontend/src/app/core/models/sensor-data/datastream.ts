/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Observation } from 'src/app/core/models/sensor-data/observation';

export class Datastream {
  observedProperty: string;
  observationType: string;
  unitOfMeasurement: string;
  observations: Observation[];
}
