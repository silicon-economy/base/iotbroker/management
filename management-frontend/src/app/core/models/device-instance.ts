/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
export class DeviceInstance {
  id?: string;
  source: string;
  tenant: string;
  deviceTypeIdentifier: string;
  registrationTime: string;
  lastSeen: string;
  enabled: boolean;
  description: string;
  hardwareRevision: string;
  firmwareVersion: string;
}
