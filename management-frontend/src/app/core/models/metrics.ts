/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
export class Metrics {
  cpuUsage: number;
  memoryUsage: number;
  receiveBandwidth: number;
  transmitBandwidth: number;
  timestamp: Date;
}
