/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
export class DeviceType {
  id?: string;
  source: string;
  identifier: string;
  providedBy: string[];
  description: string;
  enabled: boolean;
  autoRegisterDeviceInstances: boolean;
  autoEnableDeviceInstances: boolean;
}
