/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { PodHistoryValue } from 'src/app/core/models/pod-history-value.interface';

export interface PodHistory {
  name: string;
  values: PodHistoryValue[];
}
