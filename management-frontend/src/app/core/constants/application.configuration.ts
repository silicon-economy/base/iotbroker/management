/*
 * Duration of snackbars shown after errors occur
 */
export const SNACKBAR_ERROR_DURATION = 5000;
