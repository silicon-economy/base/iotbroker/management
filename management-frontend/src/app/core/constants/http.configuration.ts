import { HttpHeaders } from '@angular/common/http';

export const JSON_HEADER = new HttpHeaders({ 'Content-Type': 'application/json' });
export const HTTP_OPTIONS_JSON_HEADER = { headers: JSON_HEADER };
