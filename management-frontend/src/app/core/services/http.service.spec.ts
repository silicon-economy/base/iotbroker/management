/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Metrics } from 'src/app/core/models/metrics';
import { Pod } from 'src/app/core/models/pod.interface';
import { PodHistory } from 'src/app/core/models/pod-history.interface';
import { PodHistoryValue } from 'src/app/core/models/pod-history-value.interface';
import { Adapter } from 'src/app/core/models/adapter';
import { HttpService } from 'src/app/core/services/http.service';

describe('HttpService', () => {
  let service: HttpService;
  let httpMock: HttpTestingController;

  const managementBackendUrl = 'http://localhost:8080';

  const testAdapter1: Adapter = {
    adapterType: 'type 1', instanceCount: 5, instances: []
  };

  const testAdapter2: Adapter = {
    adapterType: 'type 2', instanceCount: 3, instances: []
  };

  const testMetrics: Metrics = {
    cpuUsage: 123,
    memoryUsage: 123,
    transmitBandwidth: 123,
    receiveBandwidth: 123,
    timestamp: new Date('2020-12-10T14:27:05Z')
  };

  const testPod1: Pod = {
    name: 'Test1', label: 'Test-Label1', values: testMetrics
  };

  const testPod2: Pod = {
    name: 'Test2', label: 'Test-Label2', values: testMetrics
  };

  const testPodHistoryValue1: PodHistoryValue = {
    value: 123, timestamp: new Date('2020-12-10T14:27:05Z')
  };

  const testPodHistoryValue2: PodHistoryValue = {
    value: 123, timestamp: new Date('2020-12-10T14:27:05Z')
  };

  const testPodHistory: PodHistory = {
    name: 'History1', values: [testPodHistoryValue1, testPodHistoryValue2]
  };

  const testPods: Pod[] = [testPod1, testPod2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpService]
    });

    service = TestBed.inject(HttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    const httpService: HttpService = TestBed.inject(HttpService);
    expect(httpService).toBeTruthy();
  });

  it('should get the correct Pod', () => {
    service.getPodByName(testPod1.name).subscribe((data: Pod) => {
      expect(data).toEqual(testPod1);
    });

    const req = httpMock.expectOne(`${managementBackendUrl}/cluster/monitoring/pods/` + testPod1.name, 'call to api');
    expect(req.request.method).toBe('GET');
    req.flush(testPod1);
    httpMock.verify();
  });

  it('should get the correct Pods', () => {
    service.getPods().subscribe((data: Pod[]) => {
      const dataArray: Pod[] = [];
      for (const pod of data) {
        dataArray.push(pod);
      }
      expect(dataArray).toEqual(testPods);
    });

    const req = httpMock.expectOne(`${managementBackendUrl}/cluster/monitoring/pods`, 'call to api');
    expect(req.request.method).toBe('GET');
    req.flush(testPods);
    httpMock.verify();
  });

  it('should get the correct Pod History', () => {
    service.getPodHistory(testPod1.name, 'receiveBandwidth', 600, 60).subscribe((data: PodHistory) => {
      expect(data).toEqual(testPodHistory);
    });

    const req = httpMock.expectOne(
        `${managementBackendUrl}/cluster/monitoring/pods/Test1/history/receiveBandwidth?range=600&stepWidth=60`,
        'call to api');
    expect(req.request.method).toBe('GET');
    req.flush(testPodHistory);
    httpMock.verify();
  });

  it('should get the correct adapter', () => {
    service.getAllAdapter().subscribe((data: Adapter[]) => {
      const dataArray: Adapter[] = [];
      for (const adapter of data) {
        dataArray.push(adapter);
      }
      expect(dataArray).toEqual([testAdapter1, testAdapter2]);
    });

    const req = httpMock.expectOne(`${managementBackendUrl}/cluster/monitoring/adapters`, 'call to api');
    expect(req.request.method).toBe('GET');
    req.flush([testAdapter1, testAdapter2]);
    httpMock.verify();
  });

});
