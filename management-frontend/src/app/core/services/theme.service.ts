/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private readonly ModeStorageKey = 'current-mode';
  private readonly ThemeStorageKey = 'current-theme';
  private readonly Modes = [
    { name: 'dark-theme', icon: 'dark_mode' },
    { name: 'light-theme', icon: 'light_mode' }
  ];
  private readonly Themes = [
    { name: 'blue', label: 'Blue' },
    { name: 'green', label: 'Green' },
    { name: 'lightblue', label: 'Light Blue' },
    { name: 'darkblue', label: 'Dark Blue' },
    { name: 'lightgreen', label: 'Light Green' },
    { name: 'darkgreen', label: 'Dark Green' },
    { name: 'yellow', label: 'Yellow' },
    { name: 'red', label: 'Red' },
  ];
  private modeState: string = localStorage.getItem(this.ModeStorageKey);
  private themeState: string = localStorage.getItem(this.ThemeStorageKey);

  // Initializes mode/theme from local storage
  initMode(): void {
    this.setMode(this.modeState);
  }

  initTheme(): void {
    this.setTheme(this.themeState);
  }

  // Resets mode/theme to first entry in resp. array
  resetMode(): void {
    this.modeState = this.Modes[0].name;
  }

  resetTheme(): void {
    this.themeState = this.Themes[0].name;
  }

  // Returns currently string-identifier for selected mode/theme
  getModeStr(): string {
    if (this.Modes.find(x => x.name == this.modeState) === undefined) {
      console.warn('Selected mode name does not match available mode names.');
    }
    return this.modeState;
  }

  getThemeStr(): string {
    if (this.Themes.find(x => x.name == this.themeState) === undefined) {
      console.warn('Selected theme name does not match available theme names.');
    }
    return this.themeState;
  }

  // Returns currently selected mode/theme object from resp. array
  getModeObj() {
    const mode = this.Modes.find(x => x.name == this.modeState);
    if (mode === undefined) {
      console.warn('Selected mode name does not match available mode names. Return undefined.');
    }
    return mode;
  }

  getThemeObj() {
    const theme = this.Themes.find(x => x.name == this.themeState);
    if (theme === undefined) {
      console.warn('Selected theme name does not match available theme names. Return undefined.');
    }
    return theme;
  }

  // Returns sequent Mode from Modes-Array
  getNextModeObj() {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeState));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    return this.Modes[nextIndex];
  }

  // Returns sequent icon-string-identifier
  getNextModeIcon(): string {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeState));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    return this.Modes[nextIndex].icon;
  }

  // Updates mode/theme in service-field and localStorage
  setMode(val: string): void {
    if (this.Modes.find(x => x.name == val) === undefined) {
      console.warn('Selected mode-name does not match available mode names. Switching to first correct entry instead');
      this.modeState = this.Modes[0].name;
      localStorage.setItem(this.ModeStorageKey, this.Modes[0].name);
    } else {
      this.modeState = val;
      localStorage.setItem(this.ModeStorageKey, val);
    }
  }

  setTheme(val: string): void {
    if (this.Themes.find(x => x.name == val) === undefined) {
      console.warn('Selected theme-name does not match available theme names. Switching to first correct entry' +
          ' instead');
      this.themeState = this.Themes[0].name;
      localStorage.setItem(this.ThemeStorageKey, this.Themes[0].name);
    } else {
      this.themeState = val;
      localStorage.setItem(this.ThemeStorageKey, val);
    }
  }

  // Cycles modes
  cycleMode() {
    this.modeState = localStorage.getItem(this.ModeStorageKey);
    if (this.Modes.find(x => x.name == this.modeState) === undefined) {
      console.warn('Currently selected mode-name does not match available mode names. Switching to available entry' +
          ' from Database.');
      this.setMode(this.Modes[0].name);
    } else {
      const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeState));
      const nextIndex = (currentIndex + 1) % this.Modes.length;
      this.setMode(this.Modes[nextIndex].name);
    }
  }

  // Returns all available modes/themes
  getAvailableModes() {
    return this.Modes;
  }

  getAvailableThemes() {
    return this.Themes;
  }

  // Provides static key for localStorage
  getModeStorageKey(): string {
    return this.ModeStorageKey;
  }

  getThemeStorageKey(): string {
    return this.ThemeStorageKey;
  }

}
