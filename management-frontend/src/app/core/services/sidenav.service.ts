/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private readonly storageKey = 'sidenav-state';

  private sidenavState = true;

  resetSidenavState(): void {
    this.sidenavState = true;
  }

  // Initialize sidenav state from local storage
  initSidenavState(): void {
    if (localStorage.getItem(this.storageKey) != undefined) {
      this.sidenavState = (localStorage.getItem(this.storageKey) == 'true');
    }
  }

  getSidenavState(): boolean {
    return this.sidenavState;
  }

  setSidenavState(state: boolean): void {
    this.sidenavState = state;
    localStorage.setItem(this.storageKey, String(state));
  }

  toggleSidenavState(): void {
    this.setSidenavState(!this.getSidenavState());
    localStorage.setItem(this.storageKey, String(this.getSidenavState()));
  }

  getStorageKey() {
    return this.storageKey;
  }
}
