/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adapter } from 'src/app/core/models/adapter';
import { Pod } from 'src/app/core/models/pod.interface';
import { PodHistory } from 'src/app/core/models/pod-history.interface';
import { environment } from 'src/app/environments/environment';

/**
 * This service communicates with the management backend through http
 */
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  private static readonly MANAGEMENT_BACKEND_URL = `${environment.managementBackendUrl}`;

  /**
   * This method returns all Pods.
   * @return The pods as an observable
   */
  getPods(): Observable<Pod[]> {
    const url = HttpService.MANAGEMENT_BACKEND_URL + '/cluster/monitoring/pods';
    return this.http.get<Pod[]>(url);
  }

  /**
   * This method returns a specific Pod.
   * @param name The name of the pod
   * @return The pod as an observable
   */
  getPodByName(name: string): Observable<Pod> {
    const url = HttpService.MANAGEMENT_BACKEND_URL + '/cluster/monitoring/pods/' + name;
    return this.http.get<Pod>(url);
  }

  /**
   * This method returns the history of a specific pod
   * @param name The name of the pod
   * @param metricName The metric that is retrieved
   * @param range The range of the metric
   * @param stepWidth The step width of the metric
   * @return The history as an observable
   */
  getPodHistory(name: string, metricName: string, range: number, stepWidth: number): Observable<PodHistory> {
    const url = HttpService.MANAGEMENT_BACKEND_URL + '/cluster/monitoring/pods/' + name + '/history/' + metricName + '?range='
        + range + '&stepWidth=' + stepWidth;
    return this.http.get<PodHistory>(url);
  }

  /**
   * This method retrieves all adapter
   * @return The adapter as an observable
   */
  getAllAdapter(): Observable<Adapter[]> {
    const url = HttpService.MANAGEMENT_BACKEND_URL + '/cluster/monitoring/adapters';
    return this.http.get<Adapter[]>(url);
  }
}
