/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Injectable, NgZone } from '@angular/core';
import { auditTime } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';


export interface ElementWatcher {
  dispose: () => void;
  change$: Observable<{ width: number, height: number, posX: number, posY: number }>;
}

/**
 *  Service for watching the position and size of HTMLElements and emitting the changes
 *  Use createWatcher() for setting up the Element to watch
 */
@Injectable({
  providedIn: 'root'
})
export class ElementWatcherService {
  constructor(private zone: NgZone) {
  }

  /**
   * returns a dispose method for cleanup and a change$ observable to subscribe to
   * @param node HTMLElement to watch
   * @param throttleTime optional time in ms to delay subscription updates
   */
  createWatcher(node: HTMLElement, throttleTime = 50): ElementWatcher {
    let width;
    let height;
    let posX;
    let posY;
    let animationFrameId;

    const _change = new Subject<{ width: number, height: number, posX: number, posY: number }>();

    const watchOnFrame = () => {
      const currentWidth = node.clientWidth;
      const currentHeight = node.clientHeight;
      const currentX = node.getBoundingClientRect().x;
      const currentY = node.getBoundingClientRect().y;

      if (currentWidth !== width || currentHeight !== height || currentX !== posX || currentY !== posY) {
        width = currentWidth;
        height = currentHeight;
        posX = currentX;
        posY = currentY;
        _change.next({ width, height, posX, posY });
      }
      animationFrameId = requestAnimationFrame(watchOnFrame);
    };

    this.zone.runOutsideAngular(watchOnFrame);


    const dispose = () => {
      cancelAnimationFrame(animationFrameId);
      _change.complete();
    };

    const _change$ = _change.asObservable();

    let change$: Observable<{ width: number, height: number, posX: number, posY: number }>;

    if (throttleTime > 0) {
      change$ = _change$.pipe(auditTime(throttleTime));
    } else {
      change$ = _change$;
    }

    return { dispose, change$ };
  }
}
