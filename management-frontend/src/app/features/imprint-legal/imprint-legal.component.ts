/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-imprint-legal',
  templateUrl: './imprint-legal.component.html',
  styleUrls: ['./imprint-legal.component.scss']
})
export class ImprintLegalComponent {
}
