/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprintLegalComponent } from 'src/app/features/imprint-legal/imprint-legal.component';

describe('AboutComponent', () => {
  let component: ImprintLegalComponent;
  let fixture: ComponentFixture<ImprintLegalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImprintLegalComponent]
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprintLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
