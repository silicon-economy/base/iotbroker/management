/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Adapter } from 'src/app/core/models/adapter';
import { HttpService } from 'src/app/core/services/http.service';
import { OverlayService } from 'src/app/core/services/overlay.service';

/**
 * This component shows the adapter in a mat accordion
 */
@Component({
  selector: 'app-adapter-overview',
  templateUrl: './adapter-overview.component.html',
  styleUrls: ['./adapter-overview.component.scss']
})
export class AdapterOverviewComponent implements OnInit, AfterViewInit, OnDestroy {

  adapterList: Adapter[];

  @ViewChild('dataContainer') container: ElementRef;

  constructor(
      private httpService: HttpService,
      public overlayService: OverlayService,
  ) {
  }


  ngAfterViewInit() {
    this.overlayService.setup(this.container.nativeElement);
    this.overlayService.startLoadingAnimation();
  }


  ngOnInit() {
    this.adapterList = [];
    this.addAdapter();
  }

  ngOnDestroy() {
    this.overlayService.cleanup();
  }

  /**
   * Retrieves the adapter through the http service and adds them to the mat accordion
   */
  addAdapter() {
    this.httpService.getAllAdapter().subscribe(adapterData => {
          this.adapterList = [];
          for (const adapter of adapterData) {
            this.adapterList.push(adapter);
          }
          this.overlayService.stopLoadingAnimation();
        },
        error => {
          console.error(error);
          this.overlayService.stopLoadingAnimation();
        });
  }
}

