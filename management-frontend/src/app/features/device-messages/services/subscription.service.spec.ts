/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { TestBed } from '@angular/core/testing';

import { SubscriptionService } from 'src/app/features/device-messages/services/subscription.service';
import { StompService } from 'src/app/features/device-messages/services/stomp/stomp.service';
import { of } from 'rxjs';
import { SensorDataMessage } from 'src/app/core/models/sensor-data/sensor-data-message';

describe('SubscriptionService', () => {
  let service: SubscriptionService;

  let mockService;
  const spyService = jasmine.createSpyObj('StompService', ['subscribeToTopic', 'sendMessage', 'unsubscribeFromTopic']);

  const testMessage: SensorDataMessage = {
    id: '1',
    originId: null,
    source: 'source',
    tenant: 'tenant',
    timestamp: null,
    location: null,
    datastreams: null
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: StompService, useValue: spyService }
      ]
    });

    mockService = TestBed.get(StompService);
    mockService.subscribeToTopic.and.returnValue(of(null));

    service = TestBed.inject(SubscriptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should subscribe', () => {
    spyOn(service, 'topicChanged');
    service.subscribeToTopic('source', 'tenant');
    const topic = '/messages/sources/source/tenants/tenant';
    expect(spyService.subscribeToTopic).toHaveBeenCalledWith(topic);
    expect(service.subscriptions.size).toEqual(1);
    expect(service.topicChanged).toHaveBeenCalled();
  });

  it('should subscribe to only source', () => {
    service.subscribeToTopic('source', '');
    const topic = '/messages/sources/source/tenants/';
    expect(spyService.subscribeToTopic).toHaveBeenCalledWith(topic);

    service.subscribeToTopic('source', undefined);
    expect(spyService.subscribeToTopic).toHaveBeenCalledWith(topic);
  });

  it('should unsubscribe', () => {
    service.subscribeToTopic('source', 'tenant');
    expect(service.subscriptions.size).toEqual(1);
    const topic = 'sources/source/tenants/tenant';
    service.unsubscribe(topic);
    expect(spyService.unsubscribeFromTopic).toHaveBeenCalled();
    expect(service.subscriptions.size).toEqual(0);
  });

  it('should return all topics', () => {
    service.subscribeToTopic('source', 'tenant');
    service.subscribeToTopic('source2', 'tenant2');
    expect(service.getTopics().length).toEqual(2);
  });

  it('should not subscribe to existing topic', () => {
    service.subscribeToTopic('source', 'tenant');
    expect(service.subscribeToTopic('source', 'tenant')).toEqual(null);
  });

  it('should push the messages', () => {
    spyOn(service, 'newMessages');
    mockService.subscribeToTopic.and.returnValue(of({ body: JSON.stringify([testMessage, testMessage]) }));
    service.subscribeToTopic('source', 'tenant');
    expect(service.newMessages).toHaveBeenCalled();
  });
});
