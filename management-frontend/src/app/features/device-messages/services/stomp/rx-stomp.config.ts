/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { RxStompConfig } from '@stomp/rx-stomp';
import { environment } from 'src/app/environments/environment';

export const rxStompConfig: RxStompConfig = {
  // Which server?
  brokerURL: `${environment.sensorDataSubscriptionServiceWebsocketUrl}`,

  // Headers
  // Typical keys: login, passcode, host
  connectHeaders: {
    login: 'guest',
    passcode: 'guest',
  },

  // How often to heartbeat?
  // Interval in milliseconds, set to 0 to disable
  heartbeatIncoming: 0, // Typical value 0 - disabled
  heartbeatOutgoing: 20000, // Typical value 20000 - every 20 seconds

  // Wait in milliseconds before attempting auto reconnect
  // Set to 0 to disable
  // Typical value 500 (500 milli seconds)
  reconnectDelay: 200,

};

export const ATTEMPTS_BEFORE_CHANGING_MIDDLE_DELAY = 10;
export const ATTEMPTS_BEFORE_CHANGING_HIGH_DELAY = 20;
export const MIDDLE_DELAY = 1000;
export const HIGH_DELAY = 10000;
