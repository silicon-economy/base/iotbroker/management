/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { RxStompService } from 'src/app/features/device-messages/services/stomp/rx-stomp.service';
import { IMessage } from '@stomp/stompjs';
import { RxStompState } from '@stomp/rx-stomp';
import { filter } from 'rxjs/operators';
import { ATTEMPTS_BEFORE_CHANGING_HIGH_DELAY, ATTEMPTS_BEFORE_CHANGING_MIDDLE_DELAY, HIGH_DELAY, MIDDLE_DELAY, rxStompConfig } from 'src/app/features/device-messages/services/stomp/rx-stomp.config';

@Injectable({
  providedIn: 'root'
})
export class StompService {

  constructor(private rxStompService: RxStompService) {
    this.changeReconnectionDelay();
  }

  public subscribeToTopic(topic: string): Observable<IMessage> {
    const unsubHeader = () => {
      return { destination: topic };
    };
    return this.rxStompService.watch({ destination: topic, unsubHeaders: unsubHeader });
  }

  public sendMessage(message: string, topic: string): void {
    this.rxStompService.publish({ destination: topic, body: JSON.stringify(message) });
  }

  public unsubscribeFromTopic(topic: Subscription): void {
    topic.unsubscribe();
  }

  /**
   * Change the reconnection delay according to the rx-stomp.config.ts
   * After a successful connection the counter will be resetted
   */
  public changeReconnectionDelay() {

    let counter = 0;

    const disconnected$ = this.rxStompService.connectionState$.pipe(
        filter((currentState: RxStompState) => {
          return currentState === RxStompState.CLOSED;
        })
    );

    const connected$ = this.rxStompService.connectionState$.pipe(
        filter((currentState: RxStompState) => {
          return currentState === RxStompState.OPEN;
        })
    );

    disconnected$.subscribe(() => {
      // will be called for every disconnect
      if (counter <= ATTEMPTS_BEFORE_CHANGING_HIGH_DELAY) {
        if (counter === ATTEMPTS_BEFORE_CHANGING_MIDDLE_DELAY) {
          rxStompConfig.reconnectDelay = MIDDLE_DELAY;
          this.rxStompService.configure(rxStompConfig);
        }
        if (counter === ATTEMPTS_BEFORE_CHANGING_HIGH_DELAY) {
          rxStompConfig.reconnectDelay = HIGH_DELAY;
          this.rxStompService.configure(rxStompConfig);
        }
        counter += 1;
      }
    });

    // reset counter after successful connection
    connected$.subscribe(() => {
      counter = -1;
    });

  }
}
