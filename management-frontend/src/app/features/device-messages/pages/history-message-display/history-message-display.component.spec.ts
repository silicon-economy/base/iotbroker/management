/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HistoryMessageDisplayComponent } from 'src/app/features/device-messages/pages/history-message-display/history-message-display.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SensorDataMessage } from 'src/app/core/models/sensor-data/sensor-data-message';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';
import { MatTableModule } from '@angular/material/table';
import { AutofillSelectStubComponent } from 'src/app/shared/components/autofill-select/autofill-select-stub.component.spec';
import { DeviceInstanceSelectionComponent } from 'src/app/features/device-messages/components/device-instance-selection/device-instance-selection.component';
import { MatIconModule } from '@angular/material/icon';
import { SensorDataService } from 'src/app/features/device-messages/services/sensor-data.service';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('HistoryMessageDisplayComponent', () => {
  let component: HistoryMessageDisplayComponent;
  let fixture: ComponentFixture<HistoryMessageDisplayComponent>;

  let mockSensorDataService;
  let mockChild;
  let mockTableService;

  const spySensorDataService = jasmine.createSpyObj('SensorDataService', ['getDeviceMessages']);
  const tableServiceSpy = jasmine.createSpyObj('TableConfigService', ['initialiseTableColumnConfig',
  'createTableColumnConfigElement']);
  const spyOverlay = jasmine.createSpyObj('OverlayService', ['setup', 'startLoadingAnimation', 'stopLoadingAnimation', 'cleanup']);
  const spySnackbar = jasmine.createSpyObj('MatSnackBar', ['open']);
  const childSpy = jasmine.createSpyObj('TopicSelectionComponent', ['getSource', 'getTenant']);

  // Test Data
  const testSource = new FormControl('source');
  const testTenant = new FormControl('tenant');

  const testMessage: SensorDataMessage = {
    id: 'sensingpuck-2413:e4c0d8dd-7ff7-4e7c-b31c-e22215cb1707',
    originId: 'sensingpuck-2413:123e4567-e89b-12d3-a456-426614174000',
    source: 'sensingpuck',
    tenant: '2413',
    timestamp: '2020-11-12T12:56:55Z',
    location: null,
    datastreams: [
      {
        observedProperty: 'property',
        observationType: 'MEASUREMENT',
        unitOfMeasurement: 'unit',
        observations: [
          {
            timestamp: '2020-11-12T12:56:55Z',
            result: 3.142,
            location: null,
            parameters: null
          }
        ]
      }
    ]
  };

  const testMessages = [testMessage, testMessage];

  const testTableColumnConfig = {
    dataAttributeToDisplay: 'test',
    header(): string {
      return 'test';
    }
  };

  beforeEach((() => {

    TestBed.configureTestingModule({
      imports: [
        MatSelectModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatAutocompleteModule,
        MatInputModule,
        MatTableModule,
        MatInputModule,
        MatIconModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      providers: [
        { provide: SensorDataService, useValue: spySensorDataService },
        { provide: OverlayService, useValue: spyOverlay },
        { provide: MatSnackBar, useValue: spySnackbar },
        { provide: DeviceInstanceSelectionComponent, useValue: childSpy },
        { provide: TableConfigService, useValue: tableServiceSpy }
      ],
      declarations: [HistoryMessageDisplayComponent, MessagesTableComponent, AutofillSelectStubComponent,
        DeviceInstanceSelectionComponent, ExpandableTableComponent]
    });

    // mock service
    mockSensorDataService = TestBed.inject(SensorDataService);
    mockSensorDataService.getDeviceMessages.and.returnValue(of(testMessages));

    mockChild = TestBed.inject(DeviceInstanceSelectionComponent);
    mockChild.getSource.and.returnValue(testSource);
    mockChild.getTenant.and.returnValue(testTenant);

    mockTableService = TestBed.inject(TableConfigService);
    mockTableService.initialiseTableColumnConfig.and.returnValue([]);
    mockTableService.createTableColumnConfigElement.and.returnValue(testTableColumnConfig);

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryMessageDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.topicSelection = mockChild;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fill the table', () => {
    spyOn(component.table, 'addMessages');
    component.showMessages();
    expect(component.table.addMessages).toHaveBeenCalled();
    expect(spySensorDataService.getDeviceMessages).toHaveBeenCalled();
  });

  it('should show snackbar when the data could not be loaded', () => {
    spyOn(component.table, 'addMessages');
    mockSensorDataService.getDeviceMessages.and.returnValue(throwError({ status: 404 }));
    component.showMessages();
    expect(component.table.addMessages).toHaveBeenCalledTimes(0);
    expect(spySnackbar.open).toHaveBeenCalled();
  });

  it('should show snackbar and not calling the table when no messages could be found', () => {
    spyOn(component.table, 'addMessages');
    mockSensorDataService.getDeviceMessages.and.returnValue(of([]));
    component.showMessages();
    expect(spySnackbar.open).toHaveBeenCalled();
    expect(component.table.addMessages).toHaveBeenCalledTimes(0);
  });
});
