/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LiveMessageDisplayComponent } from 'src/app/features/device-messages/pages/live-message-display/live-message-display.component';
import { of, Subject } from 'rxjs';
import { MatDialogModule } from '@angular/material/dialog';
import { SubscriptionService } from 'src/app/features/device-messages/services/subscription.service';
import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('LiveMessageDisplayComponent', () => {
  let component: LiveMessageDisplayComponent;
  let fixture: ComponentFixture<LiveMessageDisplayComponent>;

  let mockSubscriptionService;
  let mockTableService;

  const spyDialog = jasmine.createSpyObj('MatDialog', ['open']);
  const tableServiceSpy = jasmine.createSpyObj('TableConfigService', ['initialiseTableColumnConfig',
  'createTableColumnConfigElement']);
  const spySubscriptionService = {
    ...
        jasmine.createSpyObj('SubscriptionService', ['getTopics', 'listenToTopic']),
    topicCountNotifier: new Subject(),
    messageNotifier: new Subject(),
  };

  const tableChild = jasmine.createSpyObj('MessagesTableComponent', ['showMessages']);

  const testTableColumnConfig = {
    dataAttributeToDisplay: 'test',
    header(): string {
      return 'test';
    }
  };
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatPaginatorModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),],
      providers: [
        { provide: MatDialogModule, useValue: spyDialog },
        { provide: SubscriptionService, useValue: spySubscriptionService },
        {provide: MessagesTableComponent, useValue: tableChild},
        { provide: TableConfigService, useValue: tableServiceSpy }
      ],
      declarations: [LiveMessageDisplayComponent, MessagesTableComponent, ExpandableTableComponent]
    });

    // mock service
    mockSubscriptionService = TestBed.get(SubscriptionService);
    mockSubscriptionService.getTopics.and.returnValue(['newTopic']);
    mockSubscriptionService.listenToTopic.and.returnValue(of([]));

    mockTableService = TestBed.inject(TableConfigService);
    mockTableService.initialiseTableColumnConfig.and.returnValue([]);
    mockTableService.createTableColumnConfigElement.and.returnValue(testTableColumnConfig);

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveMessageDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
