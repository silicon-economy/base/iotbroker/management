/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { AfterContentInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TopicSubscriptionComponent } from 'src/app/features/device-messages/components/topic-subscription/topic-subscription.component';
import { SubscriptionService } from 'src/app/features/device-messages/services/subscription.service';
import { Subscription } from 'rxjs';
import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';

/**
 * This component shows all messages from the subscribed devices
 */
@Component({
  selector: 'app-live-message-display',
  templateUrl: './live-message-display.component.html',
  styleUrls: ['./live-message-display.component.scss']
})
export class LiveMessageDisplayComponent implements OnDestroy, AfterContentInit {

  @ViewChild(MessagesTableComponent) table: MessagesTableComponent;

  constructor(private subscriptionService: SubscriptionService,
              public dialog: MatDialog) {
  }

  // Topics
  public topicCounter: number;
  private dialogRef: MatDialogRef<TopicSubscriptionComponent>;
  // notifies about topic changes
  private topicCountNotifierSubscription: Subscription;
  // notifies about new messages
  private messageNotifierSubscription: Subscription;

  public ngAfterContentInit(): void {
    this.topicCounter = this.subscriptionService.getTopics().length;

    this.listenForMessages();
    this.listenForTopicChanges();
  }

  /**
   * unsubscribes from all subscriptions
   * unsubscribes from {@link topicCountNotifierSubscription}
   * disconnects from websocket
   */
  public ngOnDestroy(): void {
    this.topicCountNotifierSubscription.unsubscribe();
    this.messageNotifierSubscription.unsubscribe();
  }

  public openSubscriptionDialog(): void {
    this.dialogRef = this.dialog.open(TopicSubscriptionComponent, {
      width: '70%',
      height: '70%',
      data: {
        subscriptionService: this.subscriptionService
      }
    });
  }

  /**
   * gets notified about topic changes through {@link topicCountNotifierSubscription}
   * when a topic is added the method subscribes to it and shows the new messages
   */
  private listenForTopicChanges(): void {
    this.topicCountNotifierSubscription = this.subscriptionService.topicCountNotifier.subscribe((topics) => {
      this.topicCounter = topics;
    });
  }

  /**
   * gets notified about new messages through {@link messageNotifierSubscription}
   * shows the new messages
   */
  private listenForMessages() {
    this.messageNotifierSubscription = this.subscriptionService.messageNotifier.subscribe((result) => {
      this.table.addMessages(result);
    });
  }

}
