/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { TableColumnConfiguration } from 'src/app/shared/components/expandable-table/models/table-column-configuration.interface';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';
import { TableSensorDataMessage } from 'src/app/features/device-messages/models/table-sensor-data-message';

@Component({
  selector: 'app-messages-table',
  templateUrl: './messages-table.component.html',
  styleUrls: ['./messages-table.component.scss']
})
export class MessagesTableComponent implements OnInit{

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(ExpandableTableComponent) table: ExpandableTableComponent<TableSensorDataMessage>;

  // the attributes of the table objects that are displayed
  public dataAttributesToDisplay = ['id', 'timestamp', 'source', 'tenant', 'datastreams', 'observations'];
  public tableColumnConfig: TableColumnConfiguration[] = [];
  public i18nPrefix = 'historymessagedisplay.th_';

  constructor(private tableConfigService: TableConfigService) {
  }

  ngOnInit() {
    this.tableColumnConfig = this.tableConfigService.initialiseTableColumnConfig(this.dataAttributesToDisplay, this.i18nPrefix);
  }

  public addMessages(messages): void {
    for (const message of messages) {
      this.table.addElementToBeginning(TableSensorDataMessage.transformSensorDataMessageIntoTableMessage(message));
    }
  }

  public clearMessages(): void {
    this.table.clearElements();
  }

}
