/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';
import { SensorDataMessage } from 'src/app/core/models/sensor-data/sensor-data-message';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';
import { TableSensorDataMessage } from 'src/app/features/device-messages/models/table-sensor-data-message';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('MessagesTableComponent', () => {
  let component: MessagesTableComponent;
  let fixture: ComponentFixture<MessagesTableComponent>;

  let mockTableService;
  const tableServiceSpy = jasmine.createSpyObj('TableConfigService', ['initialiseTableColumnConfig',
  'createTableColumnConfigElement']);

  // Test Data
  const testMessage: SensorDataMessage = {
    id: 'sensingpuck-2413:e4c0d8dd-7ff7-4e7c-b31c-e22215cb1707',
    originId: 'sensingpuck-2413:123e4567-e89b-12d3-a456-426614174000',
    source: 'sensingpuck',
    tenant: '2413',
    timestamp: '2020-11-12T12:56:55Z',
    location: null,
    datastreams: [
      {
        observedProperty: 'property',
        observationType: 'MEASUREMENT',
        unitOfMeasurement: 'unit',
        observations: [
          {
            timestamp: '2020-11-12T12:56:55Z',
            result: 3.142,
            location: null,
            parameters: null
          }
        ]
      }
    ]
  };

  const testTableColumnConfig = {
    dataAttributeToDisplay: 'test',
    header(): string {
      return 'test';
    }
  };

  beforeEach(waitForAsync( () => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        MatIconModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        { provide: TableConfigService, useValue: tableServiceSpy }
      ],
      declarations: [MessagesTableComponent, ExpandableTableComponent]
    });

    mockTableService = TestBed.inject(TableConfigService);
    mockTableService.initialiseTableColumnConfig.and.returnValue([]);
    mockTableService.createTableColumnConfigElement.and.returnValue(testTableColumnConfig);

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add Messages to the table in the right format', () => {
    const time = new Date(testMessage.timestamp);
    const msg: TableSensorDataMessage = {
      id: testMessage.id,
      timestamp: time.toLocaleString(),
      source: testMessage.source,
      tenant: testMessage.tenant,
      datastreams: testMessage.datastreams.length,
      observations: 1,
      content: JSON.stringify(testMessage, undefined, 2)
    };
    component.addMessages([testMessage, testMessage]);
    expect(component.table.dataSource.data[0]).toEqual(msg);
    expect(component.table.dataSource.data.length).toEqual(2);
  });
});
