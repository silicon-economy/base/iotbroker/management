/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { AfterContentInit, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';
import { SNACKBAR_ERROR_DURATION } from 'src/app/core/constants/application.configuration';

@Component({
  selector: 'app-device-instance-selection',
  templateUrl: './device-instance-selection.component.html',
  styleUrls: ['./device-instance-selection.component.scss']
})
export class DeviceInstanceSelectionComponent implements AfterContentInit {

  @ViewChild('selectSource') selectSource: AutofillSelectComponent;
  @ViewChild('selectTenant') selectTenant: AutofillSelectComponent;

  @Output() buttonClick = new EventEmitter<object>();
  @Input() buttonLabel = '';
  @Input() allowWildcards: boolean;

  constructor(private deviceService: DeviceInstanceService, private snackBar: MatSnackBar) { }

  ngAfterContentInit(): void {
    this.getSources();
  }

  /**
   * calls the parent method given through @Output
   */
  callParent() {
    if(this.selectSource.element.valid && this.selectTenant.element.valid){
      this.buttonClick.emit();
    }
    else {
      this.snackBar.open('Inputs are not valid', 'Close', {
        duration: SNACKBAR_ERROR_DURATION
      });
    }
  }

  /**
   * get all sources through the {@link HttpService}
   */
  public getSources(): void {
    this.deviceService.getAllSources().subscribe((sources: string[]) => {
      this.selectSource.initializeElements(sources);
    }, error => {
      console.error(error);
      this.snackBar.open('Could not load the sources.', 'Close', { duration: SNACKBAR_ERROR_DURATION });
    });
  }

  /**
   * gets the tenants from one source through the {@link HttpService}
   */
  public getTenantsFromOneSource(): void {
    this.selectTenant.element.setValue('');
    const tenants = [];
    this.deviceService.getDevicesFromSource(this.selectSource.element.value, 0, 1000).subscribe((devices: DeviceInstance[]) => {
      for (const device of devices) {
        tenants.push(device.tenant);
      }
      this.selectTenant.initializeElements(tenants);
    }, error => {
      console.error(error);
      this.snackBar.open('Could not load the tenants.', 'Close', { duration: SNACKBAR_ERROR_DURATION });
    });
  }

  public getSource(): FormControl {
    return this.selectSource.element;
  }

  public getTenant(): FormControl {
    return this.selectTenant.element;
  }


}
