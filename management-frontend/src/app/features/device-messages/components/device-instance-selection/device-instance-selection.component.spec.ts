/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceInstanceSelectionComponent } from 'src/app/features/device-messages/components/device-instance-selection/device-instance-selection.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AutofillSelectStubComponent } from 'src/app/shared/components/autofill-select/autofill-select-stub.component.spec';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('TopicSelectionComponent', () => {
  let component: DeviceInstanceSelectionComponent;
  let fixture: ComponentFixture<DeviceInstanceSelectionComponent>;

  let mockDeviceService;
  const spyDeviceService = jasmine.createSpyObj('HttpService', ['getAllSources',
    'getDevicesFromSource']);
  const spySnackbar = jasmine.createSpyObj('MatSnackBar', ['open']);

  // Test data
  const testSource1 = 'sensingpuck';
  const testSources = [testSource1, 'testadapter'];

  const testDevice: DeviceInstance = {
    id: '123',
    source: 'sensingpuck',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID123',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 1',
    firmwareVersion: '1.0.0',
    description: 'description',
  };
  const testDevice2: DeviceInstance = {
    id: '456',
    source: 'testadapter',
    tenant: 'tenant2',
    deviceTypeIdentifier: 'ID123',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 2',
    firmwareVersion: '2.0.0',
    description: 'description',
  };

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [DeviceInstanceSelectionComponent, AutofillSelectStubComponent],
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        { provide: DeviceInstanceService, useValue: spyDeviceService },
        { provide: MatSnackBar, useValue: spySnackbar }
      ]
    });
    // mock service
    mockDeviceService = TestBed.inject(DeviceInstanceService);
    mockDeviceService.getAllSources.and.returnValue(of(testSources));
    mockDeviceService.getDevicesFromSource.and.returnValue(of([testDevice, testDevice2]));

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceInstanceSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the right sources', () => {
    expect(mockDeviceService.getAllSources).toBeDefined();
    expect(mockDeviceService.getAllSources).toHaveBeenCalled();
    component.getSources();
    expect(component.selectSource.elementList.length).toEqual(2);
  });

  it('should get the right tenants', () => {
    component.getTenantsFromOneSource();
    expect(spyDeviceService.getDevicesFromSource).toHaveBeenCalled();
    expect(component.selectTenant.elementList.length).toEqual(2);
  });

  it('should open the snackbar if the sources cant be loaded', () => {
    mockDeviceService.getAllSources.and.returnValue(throwError({ status: 404 }));
    component.getSources();
    expect(spySnackbar.open).toHaveBeenCalled();
  });

  it('should open the snackbar if the tenants cant be loaded', () => {
    mockDeviceService.getDevicesFromSource.and.returnValue(throwError({ status: 404 }));
    component.getTenantsFromOneSource();
    expect(spySnackbar.open).toHaveBeenCalled();
  });
});
