/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicSubscriptionComponent } from 'src/app/features/device-messages/components/topic-subscription/topic-subscription.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatCardModule } from '@angular/material/card';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SubscriptionService } from 'src/app/features/device-messages/services/subscription.service';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { DeviceInstanceSelectionComponent } from 'src/app/features/device-messages/components/device-instance-selection/device-instance-selection.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('TopicSubscriptionComponent', () => {
  let component: TopicSubscriptionComponent;
  let fixture: ComponentFixture<TopicSubscriptionComponent>;

  const spy = jasmine.createSpyObj('MatDialogRef', ['close']);
  let mockSubscriptionService;
  const spySubscriptionService = jasmine.createSpyObj('SubscriptionService', ['getTopics', 'subscribeToTopic', 'unsubscribe']);
  const spySnackbar = jasmine.createSpyObj('MatSnackBar', ['open']);

  const childSpy = jasmine.createSpyObj('TopicSelectionComponent', ['getSource', 'getTenant']);
  let mockChild;

  // Test data
  const testSource = new FormControl('source');
  const testTenant = new FormControl('tenant');

  beforeEach(async () => {

    TestBed.configureTestingModule(({
      imports: [
        HttpClientTestingModule,
        MatCardModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatAutocompleteModule,
        MatListModule,
        MatSelectModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: spy },
        { provide: SubscriptionService, useValue: spySubscriptionService },
        { provide: MatSnackBar, useValue: spySnackbar },
        { provide: DeviceInstanceSelectionComponent, useValue: childSpy}
      ],
      declarations: [TopicSubscriptionComponent, AutofillSelectComponent, DeviceInstanceSelectionComponent]
    }));

    mockSubscriptionService = TestBed.get(SubscriptionService);
    mockSubscriptionService.subscribeToTopic.and.returnValue('topic');
    mockSubscriptionService.getTopics.and.returnValue([]);

    mockChild = TestBed.get(DeviceInstanceSelectionComponent);
    mockChild.getSource.and.returnValue(testSource);
    mockChild.getTenant.and.returnValue(testTenant);

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.topicSelection = mockChild;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should subscribe to the correct topic', () => {
    component.subscribeToTopic();
    expect(spySubscriptionService.subscribeToTopic).toHaveBeenCalledWith(testSource.value, testTenant.value);
    expect(component.topics.length).toEqual(1);
  });

  it('should should open snackbar if subscribing failed', () => {
    mockSubscriptionService.subscribeToTopic.and.returnValue(null);
    component.subscribeToTopic();
    expect(component.topics.length).toEqual(0);
    expect(spySnackbar.open).toHaveBeenCalled();
  });

  it('should close the dialog', () => {
    component.exit();
    expect(component.dialogRef.close).toHaveBeenCalled();
  });

  it('should unsubscribe to the topics', () => {
    component.subscribeToTopic();
    expect(component.topics.length).toEqual(1);
    component.unsubscribe(component.topics[0]);

    expect(spySubscriptionService.unsubscribe).toHaveBeenCalled();
    expect(component.topics.length).toEqual(0);
  });
});
