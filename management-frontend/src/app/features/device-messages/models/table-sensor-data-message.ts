/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { SensorDataMessage } from 'src/app/core/models/sensor-data/sensor-data-message';

export class TableSensorDataMessage {
  id: string;
  timestamp: string;
  source: string;
  tenant: string;
  datastreams: number;
  observations: number;
  content: string;

  static transformSensorDataMessageIntoTableMessage(message: SensorDataMessage) {
    let nrObservations = 0;
    for (const ds of message.datastreams) {
      nrObservations += ds.observations.length;
    }
    return {
      id: message.id,
      source: message.source,
      tenant: message.tenant,
      timestamp: new Date(message.timestamp).toLocaleString(),
      content: JSON.stringify(message, undefined, 2),
      observations: nrObservations,
      datastreams: message.datastreams.length
    };
  }

}
