/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environments/environment';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { HTTP_OPTIONS_JSON_HEADER } from 'src/app/core/constants/http.configuration';

@Injectable({
  providedIn: 'root'
})
export class DeviceInstanceService {

  private static readonly DEVICE_REGISTRY_SERVICE_URL = `${environment.deviceRegistryServiceUrl}`;

  constructor(private http: HttpClient) {
  }

  /**
   * This method retrieves all sources
   * @return The sources as an observable
   */
  getAllSources(): Observable<string[]> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances/sources`;
    return this.http.get<string[]>(url);
  }

  /**
   * This method retrieves devices from a specific page with a specific size
   * @param pageIndex The page index from which the devices are retrieved
   * @param pageSize The size of the page from which the devices are retrieved
   * @return The devices as an observable
   */
  getDevices(pageIndex: number, pageSize: number): Observable<DeviceInstance[]> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances`;
    return this.http.get<DeviceInstance[]>(url, {
      params: {
        pageIndex,
        pageSize
      }
    });
  }

  /**
   * This method retrieves devices from a specific source on a specific page with a specific size
   * @param source The source from all devices
   * @param pageIndex The page index from which the devices are retrieved
   * @param pageSize The size of the page from which the devices are retrieved
   * @return All devices from the source as an observable
   */
  getDevicesFromSource(source: string, pageIndex: number, pageSize: number): Observable<DeviceInstance[]> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances`;
    return this.http.get<DeviceInstance[]>(url, {
      params: {
        pageIndex,
        pageSize,
        source
      }
    });
  }

  /**
   * Returns the number of all registered devices
   * @return The number of devices
   */
  getDevicesCount() {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances/count`;
    return this.http.get<number>(url);
  }

  /**
   * This method posts a new device
   * @param device The device that is added to the database
   */
  postDevice(device: DeviceInstance): Observable<DeviceInstance> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances`;
    const newDevice = {
      source: device.source,
      tenant: device.tenant,
      deviceTypeIdentifier: device.deviceTypeIdentifier,
      lastSeen: device.lastSeen,
      enabled: device.enabled,
      description: device.description,
      hardwareRevision: device.hardwareRevision,
      firmwareVersion: device.firmwareVersion,
    };
    return this.http.post<DeviceInstance>(url, JSON.stringify(newDevice), HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * This method updates an existing device
   * @param device The device that is updated
   */
  updateDevice(device: DeviceInstance): Observable<DeviceInstance> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances/${device.id}`;
    const newDevice = {
      deviceTypeIdentifier: device.deviceTypeIdentifier,
      lastSeen: device.lastSeen,
      enabled: device.enabled,
      description: device.description,
      hardwareRevision: device.hardwareRevision,
      firmwareVersion: device.firmwareVersion,
    };
    return this.http.put<DeviceInstance>(url, newDevice, HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * This method deletes a specific device
   * @param id The id of the device
   */
  deleteDevice(id: string) {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/deviceInstances/${id}`;
    return this.http.delete(url);
  }
}
