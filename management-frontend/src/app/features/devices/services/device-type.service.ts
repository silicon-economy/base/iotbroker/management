/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environments/environment';
import { DeviceType } from 'src/app/core/models/device-type';
import { HTTP_OPTIONS_JSON_HEADER } from 'src/app/core/constants/http.configuration';

@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService {


  private static readonly DEVICE_REGISTRY_SERVICE_URL = `${environment.deviceRegistryServiceUrl}`;

  constructor(private http: HttpClient) {
  }

  /**
   * This method retrieves device types from a specific page with a specific size
   * @param pageIndex The page index from which the device types are retrieved
   * @param pageSize The size of the page from which the device types are retrieved
   * @return The device types as an observable
   */
  getDeviceTypes(pageIndex: number, pageSize: number): Observable<DeviceType[]> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/deviceTypes`;
    return this.http.get<DeviceType[]>(url, {
      params: {
        pageIndex,
        pageSize
      }
    });
  }

  /**
   * This method retrieves device types for a given identifier
   * @param identifier The identifier for which the device types are retrieved
   * @returns The device types as an observable
   */
  getDeviceTypeByIdentifier(identifier: string): Observable<DeviceType[]> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/deviceTypes`;
    return this.http.get<DeviceType[]>(url, {
      params: {
        identifier
      }
    });
  }

  /**
   * This method retrieves the number of device types
   * @returns The number of device types as an observable
   */
  getDeviceTypeCount(): Observable<number> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/deviceTypes/count`;
    return this.http.get<number>(url);
  }

  /**
   * Posts a new device type
   * @param deviceType The new device type
   */
  postDeviceType(deviceType: DeviceType): Observable<DeviceType> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/deviceTypes`;
    const newType = {
      source: deviceType.source,
      identifier: deviceType.identifier,
      providedBy: deviceType.providedBy,
      description: deviceType.description,
      enabled: deviceType.enabled,
      autoRegisterDeviceInstances: deviceType.autoRegisterDeviceInstances,
      autoEnableDeviceInstances: deviceType.autoEnableDeviceInstances,
    };
    return this.http.post<DeviceType>(url, JSON.stringify(newType), HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * Updates an existing device type
   * @param deviceType The updated device type
   */
  updateDeviceType(deviceType: DeviceType): Observable<DeviceType> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/deviceTypes/${deviceType.id}`;
    const newType = {
      source: deviceType.source,
      identifier: deviceType.identifier,
      providedBy: deviceType.providedBy,
      description: deviceType.description,
      enabled: deviceType.enabled,
      autoRegisterDeviceInstances: deviceType.autoRegisterDeviceInstances,
      autoEnableDeviceInstances: deviceType.autoEnableDeviceInstances,
    };
    return this.http.put<DeviceType>(url, JSON.stringify(newType), HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * Deletes a specific device type
   * @param id The ID from the device type that will be deleted
   */
  deleteDeviceType(id: string) {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/deviceTypes/${id}`;
    return this.http.delete(url);
  }

}
