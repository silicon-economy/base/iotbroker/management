/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Component, Input, OnInit } from '@angular/core';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isNotEffectivelyEmptyValidator } from 'src/app/core/utils/validators';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { Observable, Subject } from 'rxjs';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';

@Component({
  selector: 'app-device-edit-form',
  templateUrl: './device-instance-edit-form.component.html',
  styleUrls: ['./device-instance-edit-form.component.scss']
})
export class DeviceInstanceEditFormComponent implements OnInit {

  @Input() device: DeviceInstance = new DeviceInstance();
  @Input() mode: FormMode = FormMode.Show;

  deviceForm: FormGroup;

  constructor(private fb: FormBuilder, private deviceTypeService: DeviceTypeService) {
  }

  public ngOnInit() {
    this.deviceForm = this.fb.group({
      source: [this.device.source],
      tenant: [this.device.tenant],
      deviceTypeIdentifier: [this.device.deviceTypeIdentifier],
      lastSeen: [this.device.lastSeen],
      enabled: [this.device.enabled],
      description: [this.device.description],
      hardwareRevision: [this.device.hardwareRevision],
      firmwareVersion: [this.device.firmwareVersion],
    });

    this.configureForm();

  }

  /**
   * Disables form fields and sets validators depending form mode
   */
  configureForm(): void {
    switch (this.mode) {
      case FormMode.Create: {
        this.deviceForm.get('source').addValidators([Validators.required, isNotEffectivelyEmptyValidator]);
        this.deviceForm.get('tenant').addValidators([Validators.required, isNotEffectivelyEmptyValidator]);
        this.deviceForm.get('deviceTypeIdentifier').addValidators([Validators.required, isNotEffectivelyEmptyValidator]);
        break;
      }
      case FormMode.Edit: {
        this.deviceForm.get('source').disable();
        this.deviceForm.get('tenant').disable();
        this.deviceForm.get('deviceTypeIdentifier').disable();
        break;
      }
      case FormMode.Show: {
        Object.keys(this.deviceForm.controls).forEach(key => {
          this.deviceForm.get(key).disable();
        });
        break;
      }
    }
  }

  get source() {
    return this.deviceForm.get('source');
  }

  get tenant() {
    return this.deviceForm.get('tenant');
  }

  get description() {
    return this.deviceForm.get('description');
  }

  get hardwareRevision() {
    return this.deviceForm.get('hardwareRevision');
  }

  get firmwareVersion() {
    return this.deviceForm.get('firmwareVersion');
  }

  get deviceTypeIdentifier() {
    return this.deviceForm.get('deviceTypeIdentifier');
  }

  get registrationTime() {
    return this.deviceForm.get('registrationTime');
  }

  get lastSeen() {
    return this.deviceForm.get('lastSeen');
  }

  get enabled() {
    return this.deviceForm.get('enabled');
  }

  // If one of the required fields is null or invalid this method returns false
  public requiredFieldsAreValid(): boolean {
    return !(this.source.value === null || this.tenant.value === null || this.deviceTypeIdentifier.value === null
        || this.source.invalid || this.deviceTypeIdentifier.invalid || this.tenant.invalid);
  }

  public submit(): Observable<DeviceInstance> {
    if (!this.requiredFieldsAreValid()) {
      return null;
    }

    const device = new DeviceInstance();
    const subject = new Subject<DeviceInstance>();

    this.deviceTypeService.getDeviceTypeByIdentifier(this.deviceTypeIdentifier.value).subscribe(types => {
      // device type identifier doesnt exist
      if (types.length == 0) {
        this.deviceForm.controls['deviceTypeIdentifier'].setErrors({ 'invalid': 'true' });
        subject.error({ 'invalid': 'true' });
        return null;
      }

      // create result device
      device.deviceTypeIdentifier = this.deviceTypeIdentifier.value;
      device.source = this.source.value;
      device.tenant = this.tenant.value;
      device.enabled = this.enabled.value;
      device.hardwareRevision = this.hardwareRevision.value;
      device.firmwareVersion = this.firmwareVersion.value;
      device.description = this.description.value;

      // updated devices already have an id and registration time
      if (this.mode === FormMode.Edit) {
        device.id = this.device.id;
        device.registrationTime = this.device.registrationTime;
      }

      // lastSeen has to be converted into ISO String without milliseconds
      if (this.lastSeen.value != null) {
        device.lastSeen = new Date(this.lastSeen.value).toISOString().replace(/[.]\d+/, '');
      }
      subject.next(device);
    });

    return subject.hasError ? null : subject.asObservable();
  }

}
