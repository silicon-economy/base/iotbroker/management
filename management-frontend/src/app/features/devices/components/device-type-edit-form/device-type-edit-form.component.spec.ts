/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DeviceTypeEditFormComponent } from 'src/app/features/devices/components/device-type-edit-form/device-type-edit-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { DeviceType } from 'src/app/core/models/device-type';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { of, Subject } from 'rxjs';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { FormControlPipe } from 'src/app/shared/pipes/form-control.pipe';
import { HttpLoaderFactory } from 'src/app/app.module';

describe('DeviceTypeEditFormComponent', () => {
  let component: DeviceTypeEditFormComponent;
  let fixture: ComponentFixture<DeviceTypeEditFormComponent>;

  let deviceTypeServiceMock;

  const deviceTypeServiceSpy = jasmine.createSpyObj('DeviceTypeService', ['getDeviceTypeByIdentifier']);

  // test data
  const testDeviceType: DeviceType = {
    id: '1',
    source: 'source',
    identifier: 'id',
    providedBy: ['x', 'y'],
    description: 'description',
    enabled: true,
    autoRegisterDeviceInstances: true,
    autoEnableDeviceInstances: false
  };

  const testDeviceType2: DeviceType = {
    id: '1',
    source: 'source',
    identifier: 'id',
    providedBy: ['x', 'y'],
    description: 'new',
    enabled: true,
    autoRegisterDeviceInstances: true,
    autoEnableDeviceInstances: false
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceTypeEditFormComponent, FormControlPipe],
      imports: [
        MatCardModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        { provide: DeviceTypeService, useValue: deviceTypeServiceSpy }
      ]
    });

    deviceTypeServiceMock = TestBed.inject(DeviceTypeService);
    deviceTypeServiceMock.getDeviceTypeByIdentifier.and.returnValue(of(testDeviceType));

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceTypeEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check the required fields', () => {
    component.mode = FormMode.Create;

    // fill required fields correctly
    component.source.setValue(testDeviceType.source);
    component.identifier.setValue(testDeviceType.identifier);

    expect(component.requiredFieldsAreValid()).toBeTrue();
  });

  it('should check the invalid required fields', () => {
    component.mode = FormMode.Create;

    // fill required fields correctly
    component.source.setValue(testDeviceType.source);

    expect(component.requiredFieldsAreValid()).toBeFalse();
  });


  it('should return the correct new device type', () => {
    deviceTypeServiceMock.getDeviceTypeByIdentifier.and.returnValue(of([]));
    component.mode = FormMode.Create;
    // fill device fields
    component.source.setValue(testDeviceType.source);
    component.identifier.setValue(testDeviceType.identifier);
    component.providedBy.setValue(testDeviceType.providedBy);
    component.description.setValue(testDeviceType.description);
    component.enabled.setValue(testDeviceType.enabled);
    component.autoRegisterDeviceInstances.setValue(testDeviceType.autoRegisterDeviceInstances);
    component.autoEnableDeviceInstances.setValue(testDeviceType.autoEnableDeviceInstances);

    const subject = new Subject<DeviceType>();
    subject.next(testDeviceType);

    expect(component.requiredFieldsAreValid()).toBeTrue();
    expect(component.submit()).toEqual(subject.asObservable());
    expect(deviceTypeServiceMock.getDeviceTypeByIdentifier).toHaveBeenCalled();
  });

  it('should return the correct updated device', () => {
    component.mode = FormMode.Edit;
    component.type = testDeviceType;
    component.ngOnInit();

    // change to second test type
    component.description.setValue(testDeviceType2.description);

    const subject = new Subject<DeviceType>();
    subject.next(testDeviceType2);

    expect(component.requiredFieldsAreValid()).toBeTrue();
    expect(component.submit()).toEqual(subject.asObservable());
    expect(deviceTypeServiceMock.getDeviceTypeByIdentifier).toHaveBeenCalled();
  });

  it('should return null if device type already exists', () => {
    deviceTypeServiceMock.getDeviceTypeByIdentifier.and.returnValue(of([testDeviceType]));
    component.mode = FormMode.Create;
    // fill required fields
    component.source.setValue(testDeviceType.source);
    component.identifier.setValue(testDeviceType.identifier);

    expect(component.requiredFieldsAreValid()).toBeTrue();
    expect(component.submit()).toBeNull();
    expect(deviceTypeServiceMock.getDeviceTypeByIdentifier).toHaveBeenCalled();
  });
});
