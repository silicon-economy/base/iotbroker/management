/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Subject } from 'rxjs';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { DeviceType } from 'src/app/core/models/device-type';
import { DeviceTypeEditFormComponent } from 'src/app/features/devices/components/device-type-edit-form/device-type-edit-form.component';
import { ConfirmDialogConfiguration } from 'src/app/shared/components/confirm-dialog/dialog-configuration/confirm-dialog-configuration.interface';

export class DeviceTypeDialogConfiguration implements ConfirmDialogConfiguration<DeviceTypeEditFormComponent> {
  contentComponentType = DeviceTypeEditFormComponent;
  mode: FormMode;
  type: DeviceType;
  confirmAction: ConfirmAction;

  constructor(mode: FormMode, type: DeviceType, confirmAction: ConfirmAction) {
    this.mode = mode;
    this.type = type;
    this.confirmAction = confirmAction;
  }

  configureContentComponent(contentComponent: DeviceTypeEditFormComponent): void{
    contentComponent.mode = this.mode;
    contentComponent.type = this.type;
  }

  dialogConfirmAction(contentComponent: DeviceTypeEditFormComponent): Subject<boolean> {
    const subject = new Subject<boolean>();
    contentComponent.submit().subscribe(result => {
      if (result === null) {
        subject.next(false);
      } else {
        subject.next(true);
        this.confirmAction(result);
      }
    });
    return subject;
  }
}

export type ConfirmAction = (deviceType: DeviceType) => void;
