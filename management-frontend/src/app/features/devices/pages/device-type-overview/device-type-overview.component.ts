/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DeviceType } from 'src/app/core/models/device-type';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationComponent } from 'src/app/shared/components/confirmation/confirmation.component';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SNACKBAR_ERROR_DURATION } from 'src/app/core/constants/application.configuration';
import { MatPaginator } from '@angular/material/paginator';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { FormControl, Validators } from '@angular/forms';
import { TableColumnConfiguration } from 'src/app/shared/components/expandable-table/models/table-column-configuration.interface';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { DeviceTypeEditFormComponent } from 'src/app/features/devices/components/device-type-edit-form/device-type-edit-form.component';
import { DeviceTypeDialogConfiguration, ConfirmAction } from 'src/app/features/devices/pages/device-type-overview/device-type-dialog-configuration';
import { DialogData } from 'src/app/shared/components/confirm-dialog/models/dialog-data.interface';
import { TranslateService } from '@ngx-translate/core';

/**
 * This component retrieves device types from the database and shows them
 * With this component device types can be created, deleted and edited
 */
@Component({
  selector: 'app-device-type-overview',
  templateUrl: './device-type-overview.component.html',
  styleUrls: ['./device-type-overview.component.scss']
})
export class DeviceTypeOverviewComponent implements AfterViewInit, OnDestroy, OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(ExpandableTableComponent) table: ExpandableTableComponent<DeviceType>;
  @ViewChild('dataContainer') container: ElementRef;

  // the attributes of the table objects that are displayed
  public dataAttributesToDisplay = ['source', 'identifier', 'enabled'];
  public tableColumnConfig: TableColumnConfiguration[] = [];
  public i18nPrefix = 'devicetypeoverview.th_';

  dialogRefDeviceType: MatDialogRef<ConfirmDialogComponent<DeviceTypeEditFormComponent>>;
  dialogRefConfirmation: MatDialogRef<ConfirmationComponent>;

  deviceTypesCount = 0;
  amountTypes = new FormControl('10', Validators.required);

  constructor(
      public dialog: MatDialog,
      private overlayService: OverlayService,
      private deviceTypeService: DeviceTypeService,
      private snackBar: MatSnackBar,
      private tableConfigService: TableConfigService,
      private translateService: TranslateService) {
  }

  public ngAfterViewInit(): void {
    // Setup overlay after view init, otherwise the nativeElement Reference will not be loaded
    this.overlayService.setup(this.container.nativeElement);
    this.table.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.tableColumnConfig = this.tableConfigService.initialiseTableColumnConfig(this.dataAttributesToDisplay, this.i18nPrefix);
  }

  public ngOnDestroy(): void {
    this.overlayService.cleanup();
  }

  public loadTypes(): void {
    if (this.amountTypes.invalid) {
      this.amountTypes.markAsTouched();
      return;
    }

    this.overlayService.startLoadingAnimation();
    this.deviceTypeService.getDeviceTypes(0, this.amountTypes.value).subscribe(result => {
      this.table.clearElements();
      this.table.addElements(result);
      this.deviceTypesCount = result.length;
    }, error => {
      this.snackBar.open(error.error, 'Close', { duration: SNACKBAR_ERROR_DURATION });
    }, () => {
      this.overlayService.stopLoadingAnimation();
    });
  }

  /**
   * Adds a new device type to the database through the http service
   * @param deviceType New DeviceType that is added
   */
  public addDeviceType(deviceType: DeviceType): void {
    this.deviceTypeService.postDeviceType(deviceType).subscribe(() => {
      this.loadTypes();
    }, error => {
      this.snackBar.open(error.error, 'Close', { duration: SNACKBAR_ERROR_DURATION });
    });
  }

  /**
   * Deletes a device type through the http service
   * @param id ID of the device type that is deleted
   */
  public deleteDeviceType(id: string): void {
    this.deviceTypeService.deleteDeviceType(id).subscribe(() => {
      this.loadTypes();
    }, error => {
      this.snackBar.open(error.error, 'Close', { duration: SNACKBAR_ERROR_DURATION });
    });
  }

  /**
   * Updates the device type through the http service
   * @param newDeviceType The updated device type
   */
  public updateDeviceType(newDeviceType: DeviceType): void {
    this.deviceTypeService.updateDeviceType(newDeviceType).subscribe(() => {
      this.loadTypes();
    }, error => {
      this.snackBar.open(error.error, 'Close', { duration: SNACKBAR_ERROR_DURATION });
    });
  }

  /**
   * Opens a mat dialog to create a new device type
   * forwards the result to createNewDeviceType method
   */
  public openCreateDeviceDialog(): void {
    const confirmAction: ConfirmAction = (deviceType: DeviceType) => this.addDeviceType(deviceType);
    const dialogData: DialogData<DeviceTypeEditFormComponent> = {
      title: this.translateService.instant('devicetypeoverview.create_type_title'),
      componentConfiguration: new DeviceTypeDialogConfiguration(FormMode.Create, new DeviceType(), confirmAction)
    };

    this.dialogRefDeviceType = this.dialog.open<ConfirmDialogComponent<DeviceTypeEditFormComponent>>(ConfirmDialogComponent, {
      width: '500px',
      data: dialogData
    });
  }

  /**
   * Opens a mat dialog to update a new device type
   * forwards the result to updateDeviceType method
   */
  public openUpdateDeviceTypeDialog(deviceType: DeviceType): void {
    const confirmAction: ConfirmAction = (dt: DeviceType) => this.updateDeviceType(dt);
    const dialogData: DialogData<DeviceTypeEditFormComponent> = {
      title: this.translateService.instant('devicetypeoverview.update_type_title'),
      componentConfiguration: new DeviceTypeDialogConfiguration(FormMode.Edit, deviceType, confirmAction)
    };

    this.dialogRefDeviceType = this.dialog.open<ConfirmDialogComponent<DeviceTypeEditFormComponent>>(ConfirmDialogComponent, {
      width: '500px',
      data: dialogData
    });
  }

  /**
   * Opens Confirmation for the deletion of the device type
   * @param id ID from device type to delete after confirmation
   */
  public openConfirmationDialog(id: string): void {
    this.dialogRefConfirmation = this.dialog.open(ConfirmationComponent, {
      data: {
        type: ConfirmationComponent.DELETE_CONFIRM
      }
    });
    this.dialogRefConfirmation.afterClosed().subscribe(result => {
      if (result) {
        this.deleteDeviceType(id);
      }
    });
  }

}
