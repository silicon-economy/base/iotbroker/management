/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { Subject } from 'rxjs';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { DeviceInstanceEditFormComponent } from 'src/app/features/devices/components/device-instance-edit-form/device-instance-edit-form.component';
import { ConfirmDialogConfiguration } from 'src/app/shared/components/confirm-dialog/dialog-configuration/confirm-dialog-configuration.interface';

export class DeviceInstanceDialogConfiguration implements ConfirmDialogConfiguration<DeviceInstanceEditFormComponent> {
  contentComponentType = DeviceInstanceEditFormComponent;
  mode: FormMode;
  device: DeviceInstance;
  confirmAction: ConfirmAction;

  constructor(mode: FormMode, device: DeviceInstance, confirmAction: ConfirmAction) {
    this.mode = mode;
    this.device = device;
    this.confirmAction = confirmAction;
  }

  configureContentComponent(contentComponent: DeviceInstanceEditFormComponent): void{
    contentComponent.mode = this.mode;
    contentComponent.device = this.device;
  }

  dialogConfirmAction(contentComponent: DeviceInstanceEditFormComponent): Subject<boolean> {
    const subject = new Subject<boolean>();
    contentComponent.submit().subscribe(result => {
      if (result === null) {
        subject.next(false);
      } else {
        subject.next(true);
        this.confirmAction(result);
      }
    });
    return subject;
  }
}

export type ConfirmAction = (deviceInstance: DeviceInstance) => void;
