/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DeviceInstanceOverviewComponent} from 'src/app/features/devices/pages/device-instance-overview/device-instance-overview.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { of } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';
import { SensorDataService } from 'src/app/features/device-messages/services/sensor-data.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { TableConfigService } from 'src/app/shared/components/expandable-table/services/table-config.service';
import { MockComponent } from 'ng-mocks';
import { DeviceInstanceEditFormComponent } from 'src/app/features/devices/components/device-instance-edit-form/device-instance-edit-form.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('DevicesOverviewComponent', () => {
  let component: DeviceInstanceOverviewComponent;
  let fixture: ComponentFixture<DeviceInstanceOverviewComponent>;

  let mockSensorDataService;
  let mockTableService;
  let mockDialog;

  const sensorDataSpy = jasmine.createSpyObj('SensorDataService', ['getLatestDeviceMessage']);
  const tableServiceSpy = jasmine.createSpyObj('TableConfigService', ['initialiseTableColumnConfig',
  'createTableColumnConfigElement']);
  const spyDialog = jasmine.createSpyObj('MatDialog', ['open', 'afterClosed']);
  const spyOverlay = jasmine.createSpyObj('OverlayService', ['setup', 'startLoadingAnimation',
    'stopLoadingAnimation', 'cleanup']);

  // Test data
  const testDevice: DeviceInstance = {
    id: '123',
    source: 'source1',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID123',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 1',
    firmwareVersion: '1.0.0',
    description: 'description',
  };
  const testDevice2: DeviceInstance = {
    id: '456',
    source: 'source2',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID321',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 2',
    firmwareVersion: '2.0.0',
    description: 'description',
  };

  const testDevice3: DeviceInstance = {
    id: '789',
    source: 'source2',
    tenant: 'tenant2',
    deviceTypeIdentifier: 'ID456',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 2',
    firmwareVersion: '2.0.0',
    description: 'description',
  };
  const testDevice4: DeviceInstance = {
    id: '111',
    source: 'source1',
    tenant: 'tenant2',
    deviceTypeIdentifier: 'ID654',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'r1',
    firmwareVersion: 'v1',
    description: 'desc1',
  };
  const testDevices: DeviceInstance[] = [testDevice, testDevice2];

  const testTableColumnConfig = {
    dataAttributeToDisplay: 'test',
    header(): string {
      return 'test';
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceInstanceOverviewComponent, MockComponent(ConfirmDialogComponent), ExpandableTableComponent,
        MockComponent(DeviceInstanceEditFormComponent)],
      imports: [
        MatToolbarModule,
        MatListModule,
        MatTableModule,
        MatCardModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        FormsModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatPaginatorModule,
        MatCardModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      providers: [
        { provide: MatDialogModule, useValue: spyDialog },
        { provide: OverlayService, useValue: spyOverlay },
        { provide: SensorDataService, useValue: sensorDataSpy },
        { provide: TableConfigService, useValue: tableServiceSpy }
      ],
    });

    mockDialog = TestBed.inject(MatDialogModule);
    mockDialog = TestBed.inject(MatDialogModule);
    mockDialog.open.and.returnValue(testDevice4);
    mockDialog.afterClosed.and.returnValue(of(testDevice3));

    mockSensorDataService = TestBed.inject(SensorDataService);
    mockSensorDataService.getLatestDeviceMessage.and.returnValue(of([]));

    mockTableService = TestBed.inject(TableConfigService);
    mockTableService.initialiseTableColumnConfig.and.returnValue([]);
    mockTableService.createTableColumnConfigElement.and.returnValue(testTableColumnConfig);

    fixture = TestBed.createComponent(DeviceInstanceOverviewComponent);

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceInstanceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add the correct device', () => {
    const service = TestBed.inject(DeviceInstanceService);
    spyOn(service, 'postDevice').and.callFake(() => {
      return of(testDevice);
    });
    spyOn(component, 'loadDevices');
    component.addDevice(testDevice);
    expect(service.postDevice).toHaveBeenCalled();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should delete the correct device', () => {
    spyOn(component, 'loadDevices');
    const service = TestBed.inject(DeviceInstanceService);
    component.deviceCount = 1;
    component.table.dataSource.data = testDevices;
    spyOn(service, 'deleteDevice').and.callFake(() => {
      return of(testDevice);
    });
    component.deleteDevice(testDevice.id);
    expect(service.deleteDevice).toHaveBeenCalled();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should update the correct device', () => {
    const service = TestBed.inject(DeviceInstanceService);
    spyOn(service, 'updateDevice').and.callFake(() => {
      return of(testDevice);
    });
    spyOn(component, 'loadDevices');
    component.table.dataSource.data = testDevices;
    component.updateDevice(testDevice);
    expect(service.updateDevice).toHaveBeenCalled();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should open Dialog', () => {
    component.openConfirmationDialog('test');
    component.openCreateDeviceDialog();
    component.openUpdateDeviceDialog(testDevice);
    expect(component.dialogRefDevice).toBeDefined();
    expect(component.dialogRefConfirmation).toBeDefined();
  });

  it('should delete the device', () => {
    spyOn(component.dialog, 'open')
        .and
        .returnValue({
          afterClosed: () => of(true)
        } as MatDialogRef<typeof component>);

    spyOn(component, 'deleteDevice');

    component.openConfirmationDialog(testDevice.id);
    expect(component.deleteDevice).toHaveBeenCalled();
  });
});
