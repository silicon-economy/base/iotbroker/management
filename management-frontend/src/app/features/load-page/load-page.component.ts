/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexStroke, ApexTooltip, ApexXAxis, ApexYAxis } from 'ng-apexcharts';
import { Pod } from 'src/app/core/models/pod.interface';
import { PodHistory } from 'src/app/core/models/pod-history.interface';
import { HttpService } from 'src/app/core/services/http.service';
import { OverlayService } from 'src/app/core/services/overlay.service';

export type ChartOptions = {
  chart: ApexChart,
  series: ApexAxisChartSeries,
  xaxis: ApexXAxis,
  yaxis: ApexYAxis,
  stroke: ApexStroke,
  dataLabels: ApexDataLabels,
  tooltip: ApexTooltip
};

export const initData = [
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
];

/**
 * This class gets Pods from the REST API and displays their loadage (Transmitted Bandwidth, Received Bandwidth, CPU Usage, RAM Usage)
 * @author jkozian
 * @verison 1.0
 */
@Component({
  selector: 'app-load-page',
  templateUrl: './load-page.component.html',
  styleUrls: ['./load-page.component.scss']
})
export class LoadPageComponent implements OnDestroy {

  /** Metricnames for the http calls */
  public readonly METRICNAME_CPU = 'cpuUsage';
  public METRICNAME_MEMORY = 'memoryUsage';
  public METRICNAME_TRANSMITBANDWIDTH = 'transmitBandwidth';
  public METRICNAME_RECEIVEBANDWIDTH = 'receiveBandwidth';

  /** Form controls for the range and step width settings */
  public rangeControl = new FormControl(10, [Validators.min(10), Validators.max(1440), Validators.required]);
  public stepWidthControl = new FormControl(1, [Validators.min(1), Validators.max(60), Validators.required]);

  /** The selected pod */
  public selectedPod: Pod;

  /** The selected pod history */
  public selectedPodHistory: PodHistory;

  /** The base option for all apex charts on load-page component */
  public baseOptions: Partial<ChartOptions>;

  /** More detailed options for the apex charts */
  public chartOptionsCPU: Partial<ChartOptions>;
  public chartOptionsMemory: Partial<ChartOptions>;
  public chartOptionsTB: Partial<ChartOptions>;
  public chartOptionsRB: Partial<ChartOptions>;

  /** All pods that come from the http call */
  public pods: Pod[];

  /** All pod histories that come from the http call */
  public podhistory: PodHistory;

  /** The components life status */
  public isAlive: boolean;

  /** All intervals for the http call cycle */
  public intervals: Subscription[];

  /** Amount of component from the same type (label) */
  public componentAmount: number;

  /** indicator for loading spinner of dropdown */
  public isLoadingPods = false;

  /** reference to the charts div for overlay */
  @ViewChild('chartsContainer') container: ElementRef;


  /**
   * Creates an instance of load page component.
   * @param httpservice
   * @param overlayService
   */
  constructor(private httpservice: HttpService, private overlayService: OverlayService) {
    this.selectedPod = { name: 'None', label: 'None', values: undefined };
    this.pods = [];
    this.intervals = [];
    this.fillPodList();
    this.initCharts();
    this.isAlive = true;
    this.rangeControl.setValue(10);
    this.stepWidthControl.setValue(1);
    this.componentAmount = 0;
  }

  /**
   * Sets component amount based on the pod labels
   */
  setComponentAmount(): void {
    this.componentAmount = 0;
    for (const pod of this.pods) {
      if (this.selectedPod.label == pod.label) {
        this.componentAmount = this.componentAmount + 1;
      }
    }
  }

  /**
   * on destroy
   */
  ngOnDestroy(): void {
    this.overlayService.cleanup();
    this.isAlive = false;
  }


  /**
   * Gets range in seconds
   * @returns range in seconds
   */
  getRangeSeconds(): number {
    return this.rangeControl.value * 60;
  }

  /**
   * Gets step width in seconds
   * @returns step width in seconds
   */
  getStepWidthSeconds(): number {
    return this.stepWidthControl.value * 60;
  }

  /**
   * Sets the range and step width settings on refresh button click
   */
  setSettings(): void {
    if (!(this.rangeControl.hasError('max') || this.stepWidthControl.hasError('min') || this.stepWidthControl.hasError('max') || this.rangeControl.hasError('min'))) {
      for (const sub of this.intervals) {
        sub.unsubscribe();
      }
      this.intervals = [];
      this.getPodMonitoring(this.selectedPod);
    }
  }

  /**
   * Fills pod list from http call
   */
  fillPodList(): void {
    this.isLoadingPods = true;
    this.httpservice.getPods().subscribe((pods: Pod[]) => {
      for (const pod of pods) {
        this.pods.push(pod);
      }
      this.isLoadingPods = false;
    });
  }

  /**
   * Gets pod monitoring for a selected pod from http call
   * @param getPod - The selected pod from the pod list
   */
  getPodMonitoring(getPod: Pod): void {
    this.httpservice.getPodByName(getPod.name).subscribe((pod: Pod) => {
      this.overlayService.setup(this.container.nativeElement);
      this.overlayService.startLoadingAnimation();
      this.selectedPod = pod;
      this.setComponentAmount();
      this.getPodHistory(this.METRICNAME_CPU, this.chartOptionsCPU);
      this.getPodHistory(this.METRICNAME_MEMORY, this.chartOptionsMemory);
      this.getPodHistory(this.METRICNAME_RECEIVEBANDWIDTH, this.chartOptionsRB);
      this.getPodHistory(this.METRICNAME_TRANSMITBANDWIDTH, this.chartOptionsTB);
    });
  }

  /**
   * Gets the pod history for the metric and chart options
   * @param metric - The metric for the http call
   * @param chartOptions - The chart option for which chart the call will be made
   */
  getPodHistory(metric: string, chartOptions: Partial<ChartOptions>): void {
    const newTimestamps: Date[] = [];
    const newData: number[] = [];
    this.httpservice.getPodHistory(this.selectedPod.name, metric, this.getRangeSeconds(), this.getStepWidthSeconds()).subscribe(history => {
      this.selectedPodHistory = history;
      for (const value of history.values) {
        newData.push(value.value);
        newTimestamps.push(value.timestamp);
      }
      chartOptions.xaxis = {
        labels: {
          formatter: (value: string) => this.dateFormatter(value)
        },
        categories: newTimestamps
      };
      chartOptions.series = [{
        name: metric, data: newData
      }];

      const cc = document.getElementById('cpuChart');
      cc.style.display = 'block';

      const mc = document.getElementById('memoryChart');
      mc.style.display = 'block';

      const tbc = document.getElementById('transmitBandwidthChart');
      tbc.style.display = 'block';

      const rbc = document.getElementById('receiveBandwidthChart');
      rbc.style.display = 'block';

      const s = document.getElementById('settings');
      s.style.display = 'block';

      this.setUpdateInterval(this.selectedPod, newData, newTimestamps, metric, chartOptions);
      this.overlayService.stopLoadingAnimation();

    }, error => {
      console.error(error);
      this.overlayService.stopLoadingAnimation();

    });
  }

  /**
   * Sets the update interval for the chart
   * @param selectedPod - The selected Pod
   * @param newData - The input data from the pod history
   * @param newTimestamps - The input timestamps from the pod history
   * @param metric - The metric for which the update interval will be set
   * @param chartOptions - The chart options to be updated
   */
  setUpdateInterval(selectedPod: Pod, newData: number[], newTimestamps: Date[], metric: string, chartOptions: Partial<ChartOptions>): void {
    const sub = interval(this.getStepWidthSeconds() * 1000).pipe(takeWhile(() => this.isAlive && this.selectedPod == selectedPod)).subscribe(() => {
      this.httpservice.getPodByName(selectedPod.name).subscribe(
          (pod: Pod) => {
            newData.shift();
            newTimestamps.shift();
            switch (metric) {
              case this.METRICNAME_CPU:
                newData.push(pod.values.cpuUsage);
                break;
              case this.METRICNAME_MEMORY:
                newData.push(pod.values.memoryUsage);
                break;
              case this.METRICNAME_TRANSMITBANDWIDTH:
                newData.push(pod.values.transmitBandwidth);
                break;
              case this.METRICNAME_RECEIVEBANDWIDTH:
                newData.push(pod.values.receiveBandwidth);
                break;
            }
            newTimestamps.push(pod.values.timestamp);
            chartOptions.xaxis = {
              labels: {
                formatter: (value: string) => this.dateFormatter(value)
              },
              categories: newTimestamps
            };
            chartOptions.series = [{
              name: metric, data: newData
            }];
          }
      );
    });
    this.intervals.push(sub);
  }

  /**
   * Formats the orignal string to be more compensated
   * @param original - The original date string
   * @returns formatted date string in format "hh:mm (UTC±hh)"
   */
  dateFormatter(original: string): string {
    const d = new Date(original);
    let s: string;
    if (d.getHours() < 10) {
      s = '0' + d.getHours();
    } else {
      s = '' + d.getHours();
    }
    if (d.getMinutes() < 10) {
      s += ':0' + d.getMinutes();
    } else {
      s += ':' + d.getMinutes();
    }
    if (d.getTimezoneOffset() == 0) {
      s += ' (UTC±' + 0 + ')';
    } else if (d.getTimezoneOffset() < 0) {
      s += ' (UTC+' + -d.getTimezoneOffset() / 60 + ')';
    } else if (d.getTimezoneOffset() > 0) {
      s += ' (UTC' + -d.getTimezoneOffset() / 60 + ')';
    }
    return s;
  }

  /**
   * Initialize all chart options
   */
  initCharts(): void {
    this.baseOptions = {
      chart: {
        height: 350,
        type: 'area',
        toolbar: {
          show: false
        },
        animations: {
          enabled: true,
          easing: 'linear',
          dynamicAnimation: {
            enabled: true,
          }
        }
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        type: 'datetime',
        labels: {
          show: true,
          formatter: (value: string) => this.dateFormatter(value)
        },
      },
    };

    this.chartOptionsCPU = { ...this.baseOptions };
    this.chartOptionsCPU.yaxis = {
      min: 0,
      show: true,
      labels: { formatter: (value: number) => (value).toFixed(4) },
      title: { text: 'CPU usage in vCPU' }
    };
    this.chartOptionsCPU.series = [{ name: 'CPU cores', data: initData }];
    this.chartOptionsCPU.tooltip = {
      y: {
        formatter: (value: number) => {
          return (value) + ' vCPU';
        }
      }
    };

    this.chartOptionsMemory = { ...this.baseOptions };
    this.chartOptionsMemory.yaxis = {
      min: 0,
      show: true,
      labels: { formatter: (value: number) => (value / 1048576).toFixed(2) },
      title: { text: 'RAM usage in MB' }
    };
    this.chartOptionsMemory.series = [{ name: 'RAM usage', data: initData }];
    this.chartOptionsMemory.tooltip = {
      y: {
        formatter: (value: number) => {
          return (value / 1048576) + ' MB';
        }
      }
    };

    this.chartOptionsTB = { ...this.baseOptions };
    this.chartOptionsTB.yaxis = {
      min: 0,
      show: true,
      labels: { formatter: (value: number) => (value / 1024).toFixed(2) },
      title: { text: 'Transmitted bandwidth in KB/s' }
    };
    this.chartOptionsTB.series = [{ name: 'Transmitted Bandwidth', data: initData }];
    this.chartOptionsTB.tooltip = {
      y: {
        formatter: (value: number) => {
          return (value / 1024) + ' KB/s';
        }
      }
    };

    this.chartOptionsRB = { ...this.baseOptions };
    this.chartOptionsRB.yaxis = {
      min: 0,
      show: true,
      labels: { formatter: (value: number) => (value / 1024).toFixed(2) },
      title: { text: 'Received bandwidth in KB/s' }
    };
    this.chartOptionsRB.series = [{ name: 'Received Bandwidth', data: initData }];
    this.chartOptionsRB.tooltip = {
      y: {
        formatter: (value: number) => {
          return (value / 1024) + ' KB/s';
        }
      }
    };
  }
}
