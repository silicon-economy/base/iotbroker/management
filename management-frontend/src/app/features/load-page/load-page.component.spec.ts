/**
 * @license
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { ComponentFixture, discardPeriodicTasks, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { LoadPageComponent } from 'src/app/features/load-page/load-page.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatIconModule } from '@angular/material/icon';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Metrics } from 'src/app/core/models/metrics';
import { Pod } from 'src/app/core/models/pod.interface';
import { PodHistoryValue } from 'src/app/core/models/pod-history-value.interface';
import { PodHistory } from 'src/app/core/models/pod-history.interface';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpService } from 'src/app/core/services/http.service';
import { OverlayService } from 'src/app/core/services/overlay.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('LoadPageComponent', () => {
  let component: LoadPageComponent;
  let fixture: ComponentFixture<LoadPageComponent>;

  let mockService;

  const spy = jasmine.createSpyObj('HttpService', ['getPods', 'getPodByName', 'getPodHistory']);
  const spyOverlay = jasmine.createSpyObj('OverlayService', ['setup', 'startLoadingAnimation', 'stopLoadingAnimation', 'cleanup']);

  // Test Data
  const testMetrics: Metrics = {
    cpuUsage: 123, memoryUsage: 123, transmitBandwidth: 123, receiveBandwidth: 123,
    timestamp: new Date('2020-12-10T14:27:05Z')
  };
  const testPod1: Pod = { name: 'Test1', label: 'Test-Label', values: testMetrics };
  const testPod2: Pod = { name: 'Test2', label: 'Test-Label', values: testMetrics };
  const testPods: Pod[] = [testPod1, testPod2];

  const testPodHistoryValue1: PodHistoryValue = { value: 123, timestamp: new Date('2020-12-10T14:27:05Z') };
  const testPodHistoryValue2: PodHistoryValue = { value: 123, timestamp: new Date('2020-12-10T14:27:05Z') };
  const testPodHistory: PodHistory = {
    name: 'History1', values:
        [testPodHistoryValue1, testPodHistoryValue2,
          testPodHistoryValue1, testPodHistoryValue2,
          testPodHistoryValue1, testPodHistoryValue2,
          testPodHistoryValue1, testPodHistoryValue2,
          testPodHistoryValue1, testPodHistoryValue2,
          testPodHistoryValue1, testPodHistoryValue2,
          testPodHistoryValue1, testPodHistoryValue2
        ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadPageComponent],
      imports: [
        HttpClientTestingModule,
        MatSelectModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        NgApexchartsModule,
        MatListModule,
        MatRadioModule,
        MatIconModule,
        MatFormFieldModule,
        BrowserModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule
      ],
      providers: [
        { provide: HttpService, useValue: spy },
        { provide: OverlayService, useValue: spyOverlay }

      ]
    });
    TestBed.compileComponents();
    mockService = TestBed.get(HttpService);
    mockService.getPods.and.returnValue(of(testPods));
    mockService.getPodByName.and.returnValue(of(testPod1));
    mockService.getPodHistory.and.returnValue(of(testPodHistory));
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component.isAlive).toBeTruthy();
  });

  it('should get all pods', () => {
    expect(mockService.getPods).toBeDefined();
    expect(mockService.getPods).toHaveBeenCalled();
    expect(component.pods.length).toBe(2);
  });

  it('should get one pod', () => {
    component.getPodMonitoring(testPod1);
    expect(mockService.getPodByName).toBeDefined();
    expect(mockService.getPodByName).toHaveBeenCalled();
    expect(component.selectedPod).toEqual(testPod1);
  });

  it('should update cpu data in interval', fakeAsync((): void => {
    component.isAlive = true;
    component.selectedPod = testPod1;
    const newData = [1, 2, 3, 4];
    const newTimestamps = [new Date('2020-12-10T14:27:05Z'), new Date('2020-12-10T14:28:05Z'), new Date('2020-12-10T14:29:05Z'), new Date('2020-12-10T14:30:05Z')];
    component.setUpdateInterval(testPod1, newData, newTimestamps, component.METRICNAME_CPU, component.chartOptionsCPU);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([1, 2, 3, 4]);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([2, 3, 4, 123]);

    discardPeriodicTasks();
    flush();
  }));

  it('should update memory data in interval', fakeAsync((): void => {
    component.isAlive = true;
    component.selectedPod = testPod1;
    const newData = [1, 2, 3, 4];
    const newTimestamps = [new Date('2020-12-10T14:27:05Z'), new Date('2020-12-10T14:28:05Z'), new Date('2020-12-10T14:29:05Z'), new Date('2020-12-10T14:30:05Z')];
    component.setUpdateInterval(testPod1, newData, newTimestamps, component.METRICNAME_MEMORY, component.chartOptionsMemory);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([1, 2, 3, 4]);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([2, 3, 4, 123]);

    discardPeriodicTasks();
    flush();
  }));

  it('should update transmit bandwidth data in interval', fakeAsync((): void => {
    component.isAlive = true;
    component.selectedPod = testPod1;
    const newData = [1, 2, 3, 4];
    const newTimestamps = [new Date('2020-12-10T14:27:05Z'), new Date('2020-12-10T14:28:05Z'), new Date('2020-12-10T14:29:05Z'), new Date('2020-12-10T14:30:05Z')];
    component.setUpdateInterval(testPod1, newData, newTimestamps, component.METRICNAME_TRANSMITBANDWIDTH, component.chartOptionsTB);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([1, 2, 3, 4]);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([2, 3, 4, 123]);

    discardPeriodicTasks();
    flush();
  }));

  it('should update receive bandwidth data in interval', fakeAsync((): void => {
    component.isAlive = true;
    component.selectedPod = testPod1;
    const newData = [1, 2, 3, 4];
    const newTimestamps = [new Date('2020-12-10T14:27:05Z'), new Date('2020-12-10T14:28:05Z'), new Date('2020-12-10T14:29:05Z'), new Date('2020-12-10T14:30:05Z')];
    component.setUpdateInterval(testPod1, newData, newTimestamps, component.METRICNAME_RECEIVEBANDWIDTH, component.chartOptionsRB);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([1, 2, 3, 4]);
    tick(component.getStepWidthSeconds() * 500);
    expect(newData).toEqual([2, 3, 4, 123]);

    discardPeriodicTasks();
    flush();
  }));

  it('should set settings', () => {
    component.setSettings();
    expect(component.intervals.length).toEqual(4);
  });

  it('should set component amount', () => {
    component.selectedPod = testPod1;
    component.setComponentAmount();
    expect(mockService.getPods).toBeDefined();
    expect(mockService.getPods).toHaveBeenCalled();
    expect(component.componentAmount).toEqual(2);
  });

});
