/**
 * @license
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-welcome-page-english',
  templateUrl: 'welcome-page-english.component.html',
  styleUrls: ['../../welcome-page.component.scss']
})
export class WelcomePageEnglishComponent {
}
