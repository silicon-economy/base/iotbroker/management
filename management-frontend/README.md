<!--
Workaround for the license-checker tool and this project's license:

The license-checker tool performs different steps to determine the license of npm packages.
When generating a third-party license report for this project, the tool tries to determine this project's license, too.
Since the Open Logistics License  is currently not contained in the SPDX License List, the license-checker tool fails and ultimately falls back to scanning the README.md file for some license information.
Strangely, the tool then chooses the first URL it can find as the license for this project.
Therefore, this comment is used to have a reliable place for the first link in this document.
In the generated license report a URL as a license makes no sense, of course, but for this project this information is not really relevant anyway.

https://openlogisticsfoundation.org

This is a known issue with the license-checker tool: https://github.com/davglass/license-checker/issues/125
 -->

# IoT Broker Management Frontend

## Versions

The versions of the runtime environment and the most relevant tools and frameworks used are listed below:

* Node: 16.15.0 (as used by the trion/ng-cli-karma:13.3.7 image used to build the application)
* npm: 8.5.5
* Angular CLI: 13.3.8

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports

This project uses the [license-checker](https://www.npmjs.com/package/license-checker) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:

`npx license-checker --unknown --csv --out ./third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`npx license-checker --unknown --summary > ./third-party-licenses/third-party-licenses-summary.txt`
